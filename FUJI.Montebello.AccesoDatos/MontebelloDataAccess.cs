﻿using FUJI.Feed2.Entidades;
using FUJI.Montebello.AccesoDatos.DataAccess;
using FUJI.Montebello.AccesoDatos.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FUJI.Montebello.AccesoDatos
{
    public class MontebelloDataAccess
    {
        public NAPOLEONEntities NapoleonDA;

        #region login
        public bool Logear(string _Usuario, String _Password, String sitio, ref clsUsuario _User)
        {
            bool Success = false;
            try
            {
                List<clsUsuario> _lstUser = new List<clsUsuario>();
                using (NapoleonDA = new NAPOLEONEntities())
                {
                    if (NapoleonDA.tbl_CAT_Usuarios.Any(x => (bool)x.bitActivo && x.vchUsuario.Trim().ToUpper() == _Usuario.Trim().ToUpper() && x.vchPassword.Trim() == _Password.Trim() && (bool)x.bitActivo))
                    {
                        var query = (from user in NapoleonDA.tbl_CAT_Usuarios
                                     join site in NapoleonDA.tbl_ConfigSitio on user.id_Sitio equals site.id_Sitio
                                     where user.vchUsuario.Trim().ToUpper() == _Usuario.Trim().ToUpper() && user.vchPassword.Trim() == _Password.Trim() && (bool)user.bitActivo && site.vchClaveSitio.ToUpper() == sitio.ToUpper()
                                     select new
                                     {
                                         intUsuarioID = user.intUsuarioID,
                                         intTipoUsuarioID = user.intTipoUsuarioID,
                                         intProyectoID = user.intProyectoID,
                                         id_Sitio = user.id_Sitio,
                                         vchSitio = site.vchnombreSitio,
                                         vchNombre = user.vchNombre,
                                         vchApellido = user.vchApellido,
                                         vchUsuario = user.vchUsuario,
                                         vchPassword = user.vchPassword
                                     }
                            ).First();
                        if (query != null)
                        {
                            if (Success = query.intUsuarioID > 0 ? true : false)
                            {
                                _User.intUsuarioID = query.intUsuarioID;
                                _User.intTipoUsuarioID = query.intTipoUsuarioID == null ? 0 : (int)query.intTipoUsuarioID;
                                _User.intProyectoID = query.intProyectoID == null ? 0 : (int)query.intProyectoID;
                                _User.id_Sitio = query.id_Sitio == null ? 0 : (int)query.id_Sitio;
                                _User.vchNombre = query.vchNombre;
                                _User.vchSitio = query.vchSitio;
                                _User.vchApellido = query.vchApellido;
                                _User.vchUsuario = query.vchUsuario;
                                _User.vchPassword = query.vchPassword;
                                _User.Token = Security.Encrypt(query.intUsuarioID + "|" + query.vchUsuario + "|" + query.vchPassword);
                            }
                        }
                    }
                    else
                    {
                        Success = false;
                    }
                }
            }
            catch (Exception eLogear)
            {
                Log.EscribeLog("Existe un error en NapoleonDataAccess.Logear: " + eLogear.Message);
                Success = false;
            }
            return Success;
        }
        #endregion login

        #region CAT
        public List<tbl_CAT_Modalidad> getCatModalidad()
        {
            List<tbl_CAT_Modalidad> _lstCat = new List<tbl_CAT_Modalidad>();
            try
            {
                using (NapoleonDA = new NAPOLEONEntities())
                {
                    if (NapoleonDA.tbl_CAT_Modalidad.Any(x => (bool)x.bitActivo))
                    {
                        var query = (NapoleonDA.tbl_CAT_Modalidad.Where(x => (bool)x.bitActivo)).ToList();
                        if (query.Count > 0)
                        {
                            _lstCat.AddRange(query);
                        }
                    }
                }
            }
            catch (Exception egC)
            {
                _lstCat = null;
                Log.EscribeLog("Existe un error al obtener el catalogo de getCatModalidad: " + egC.Message);
            }
            return _lstCat;
        }
        #endregion CAT

        #region Estudio
        public List<clsEstudio> getListEstudios(int id_Sitio, int intModalidadID, DateTime datInicio, DateTime datFinal, ref string mensaje)
        {
            List<clsEstudio> _lstEst = new List<clsEstudio>();
            try
            {
                using (NapoleonDA = new NAPOLEONEntities())
                {
                    var query = NapoleonDA.stp_getEstudioMontebello(id_Sitio, intModalidadID, datInicio, datFinal).ToList();
                    if (query != null)
                    {
                        if (query.Count > 0)
                        {
                            foreach (stp_getEstudioMontebello_Result item in query)
                            {
                                clsEstudio mdl = new clsEstudio();
                                mdl.intEstudioID = item.intEstudioID;
                                mdl.id_Sitio = (int)item.id_Sitio;
                                mdl.vchClaveSitio = item.vchClaveSitio;
                                mdl.intModalidadID = (int)item.intModalidadID;
                                mdl.vchModalidadID = item.vchModalidadClave;
                                mdl.vchAccessionNumber = item.vchAccessionNumber;
                                mdl.vchPatientBirthDate = item.vchPatientBirthDate;
                                mdl.PatientID = item.PatientID;
                                mdl.PatientName = item.PatientName;
                                mdl.vchEdad = item.vchEdad;
                                mdl.vchGenero = item.vchgenero;
                                mdl.datFecha = (DateTime)item.datFecha;
                                mdl.intEstatusID = (int)item.intEstatusID;
                                mdl.vchEstatusID = item.vchEstatusDesc;
                                mdl.vchCarpetaSyn = item.vchCarpetaSyn;
                                mdl.datFechaEstudio = (DateTime)item.datFechaEstudio;
                                //mdl.bitSolInterpretacion = item.bitSolInterpretacion == null ? false : (bool)item.bitSolInterpretacion;
                                _lstEst.Add(mdl);
                            }
                        }
                    }
                }
                mensaje = "";
            }
            catch (Exception egE)
            {
                mensaje = egE.Message;
                _lstEst = null;
                Log.EscribeLog("Existe un error en getListEstudios: " + egE.Message);
            }
            if (_lstEst.Count > 0)
            {
                Log.EscribeLog("Estucios: " + _lstEst.Count.ToString());
            }
            return _lstEst;
        }

        public List<clsEstudio> getListEstudiosParam(int id_Sitio, int intModalidadID, DateTime datInicio, DateTime datFinal, string vchAccNum, string vchPatientID, string vchPatientName, ref string mensaje)
        {
            List<clsEstudio> _lstEst = new List<clsEstudio>();
            try
            {
                using (NapoleonDA = new NAPOLEONEntities())
                {
                    var query = NapoleonDA.stp_getEstudioMontebelloParam(id_Sitio, intModalidadID, datInicio, datFinal, vchAccNum, vchPatientID, vchPatientName).ToList();
                    if (query != null)
                    {
                        if (query.Count > 0)
                        {
                            foreach (stp_getEstudioMontebelloParam_Result item in query)
                            {
                                clsEstudio mdl = new clsEstudio();
                                mdl.intEstudioID = item.intEstudioID;
                                mdl.id_Sitio = (int)item.id_Sitio;
                                mdl.vchClaveSitio = item.vchClaveSitio;
                                mdl.intModalidadID = (int)item.intModalidadID;
                                mdl.vchModalidadID = item.vchModalidadClave.TrimEnd();
                                mdl.vchAccessionNumber = item.vchAccessionNumber;
                                mdl.vchPatientBirthDate = item.vchPatientBirthDate;
                                mdl.PatientID = item.PatientID;
                                mdl.PatientName = item.PatientName;
                                mdl.vchEdad = item.vchEdad;
                                mdl.vchGenero = item.vchgenero;
                                mdl.datFecha = (DateTime)item.datFecha;
                                mdl.intEstatusID = (int)item.intEstatusID;
                                mdl.vchEstatusID = item.vchEstatusDesc;
                                mdl.vchCarpetaSyn = item.vchCarpetaSyn;
                                mdl.datFechaEstudio = (DateTime)item.datFechaEstudio;
                                //mdl.bitSolInterpretacion = item.bitSolInterpretacion == null ? false : (bool)item.bitSolInterpretacion;
                                Log.EscribeLog("DA AccNum:" + mdl.vchModalidadID + ".");
                                _lstEst.Add(mdl);
                            }
                        }
                    }
                }
                mensaje = "";
            }
            catch (Exception egE)
            {
                mensaje = egE.Message;
                _lstEst = null;
                Log.EscribeLog("Existe un error en getListEstudiosParam: " + egE.Message);
            }
            if (_lstEst.Count > 0)
            {
                Log.EscribeLog("Estucios: " + _lstEst.Count.ToString());
            }
            return _lstEst;
        }

        public clsEstudio getEstudio(int intEstudioID, ref string mensaje)
        {
            clsEstudio _mdlEst = new clsEstudio();
            try
            {
                using (NapoleonDA = new NAPOLEONEntities())
                {
                    var query = NapoleonDA.stp_getEstudioPDF(intEstudioID).First();
                    if (query != null)
                    {
                        _mdlEst.intEstudioID = query.intEstudioID;
                        _mdlEst.id_Sitio = (int)query.id_Sitio;
                        _mdlEst.vchClaveSitio = query.vchClaveSitio;
                        _mdlEst.intModalidadID = (int)query.intModalidadID;
                        _mdlEst.vchModalidadID = query.vchModalidadClave;
                        _mdlEst.vchAccessionNumber = query.vchAccessionNumber;
                        _mdlEst.vchPatientBirthDate = query.vchPatientBirthDate;
                        _mdlEst.PatientID = query.PatientID;
                        _mdlEst.PatientName = query.PatientName;
                        _mdlEst.vchEdad = query.vchEdad;
                        _mdlEst.vchGenero = query.vchgenero;
                        _mdlEst.datFecha = (DateTime)query.datFecha;
                        _mdlEst.intEstatusID = (int)query.intEstatusID;
                        _mdlEst.vchEstatusID = query.vchEstatusDesc;
                        _mdlEst.datFechaEstudio = (DateTime)query.datFechaEstudio;
                    }
                }
                mensaje = "";
            }
            catch (Exception egE)
            {
                mensaje = egE.Message;
                _mdlEst = null;
                Log.EscribeLog("Existe un error en getEstudio: " + egE.Message);
            }
            return _mdlEst;
        }

        public bool setSolicitarInterpretacion(int intEstudioID, ref string mensaje)
        {
            bool valido = false;
            try
            {
                using (NapoleonDA = new NAPOLEONEntities())
                {
                    if (NapoleonDA.tbl_MST_Estudio.Any(x => x.intEstudioID == intEstudioID))
                    {
                        tbl_MST_Estudio mdl = new tbl_MST_Estudio();
                        mdl = NapoleonDA.tbl_MST_Estudio.Where(x => x.intEstudioID == intEstudioID).First();
                        //mdl.bitSolInterpretacion = true;
                        mdl.datFecha = DateTime.Now;
                        NapoleonDA.SaveChanges();
                        valido = true;
                    }
                    else
                    {
                        valido = false;
                        mensaje = "No se encontró el estudio: " + intEstudioID;
                    }
                }
            }
            catch (Exception esSI)
            {
                mensaje = esSI.Message;
                Log.EscribeLog("Existe un error en setSolicitarInterpretacion: " + esSI.Message);
            }
            return valido;
        }
        #endregion Estudio

        #region aviso
        public tbl_MST_Avisos obtenerAvisos(int id_Sitio)
        {
            tbl_MST_Avisos aviso = new tbl_MST_Avisos();
            try
            {
                using (NapoleonDA = new NAPOLEONEntities())
                {
                    if (NapoleonDA.tbl_MST_Avisos.Any(x => x.id_Sitio == id_Sitio && (bool)x.bitActivo))
                    {
                        aviso = NapoleonDA.tbl_MST_Avisos.Where(x => x.id_Sitio == id_Sitio && (bool)x.bitActivo).First();
                    }
                }
            }
            catch (Exception esSI)
            {
                Log.EscribeLog("Existe un error en obtenerAvisos: " + esSI.Message);
            }
            return aviso;
        }

        public bool setAvisoConfirmacion(tbl_MST_Avisos aviso, ref string mensaje)
        {
            bool valido = false;
            try
            {
                using (NapoleonDA = new NAPOLEONEntities())
                {
                    if (NapoleonDA.tbl_MST_Avisos.Any(x => x.intAvisoID == aviso.intAvisoID))
                    {
                        tbl_MST_Avisos mdl = new tbl_MST_Avisos();
                        mdl = NapoleonDA.tbl_MST_Avisos.First(x => x.intAvisoID == aviso.intAvisoID);
                        mdl.bitActivo = false;
                        NapoleonDA.SaveChanges();
                        valido = true;
                    }
                    else
                    {
                        valido = false;
                        mensaje = "No se encuentra el aviso.";
                    }
                }
            }
            catch (Exception esSI)
            {
                mensaje = "Existe un error en setAvisoConfirmacion: " + esSI.Message;
                valido = false;
                Log.EscribeLog("Existe un error en setAvisoConfirmacion: " + esSI.Message);
            }
            return valido;
        }
        #endregion
    }
}
