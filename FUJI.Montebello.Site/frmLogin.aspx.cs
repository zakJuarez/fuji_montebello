﻿using FUJI.Montebello.AccesoDatos.Extensions;
using FUJI.Montebello.Site.Services;
using FUJI.Montebello.Site.Services.DataContracts;
using System;
using System.Configuration;

namespace FUJI.Montebello.Site
{
    public partial class frmLogin : System.Web.UI.Page
    {
        MontebelloService NapoleonDA = new MontebelloService();
        public string URL
        {
            get
            {
                return ConfigurationManager.AppSettings["URL"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblLogin.Text = "";
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //tbl_CAT_Usuarios _user = new tbl_CAT_Usuarios();
                LoginRequest _req = new LoginRequest();
                _req.password = Security.Encrypt(txtContrasenaUser.Text);
                string userSite = txtUsuarioUser.Text;
                string[] UserSiteS = userSite.Split('@');
                if(UserSiteS.Length == 2)
                {
                    _req.username = UserSiteS[0].ToString();
                    _req.sitio = UserSiteS[1].ToString();
                    LoginResponse access = NapoleonDA.Logear(_req);
                    if (access != null)
                    {
                        if (access.Success)
                        {
                            Session["UserIDMB"] = access.CurrentUser.vchUsuario;
                            Session["intUsuarioID"] = access.CurrentUser.intUsuarioID;
                            Session["Password"] = access.CurrentUser.vchPassword;
                            Session["intTipoUsuario"] = access.CurrentUser.intTipoUsuarioID;
                            Session["tbl_CAT_Usuarios"] = access.CurrentUser;
                            Session["Token"] = access.CurrentUser.Token;
                            Session["id_sitio"] = access.CurrentUser.id_Sitio;
                            //Session["sucOID"] = access.CurrentUser.sucOID;
                            if (access.CurrentUser.intTipoUsuarioID == 4)
                            {
                                Response.Redirect(URL + "/AgregarSitio.aspx", false);
                            }
                            else
                            {
                                Response.Redirect(URL + "/frmGeneral.aspx", false);
                            }
                            lblLogin.Text = "Inicio de Sesion correcta";
                            Log.EscribeLog("Usuario correcto. " + access.CurrentUser.vchUsuario);
                            Log.EscribeLog(URL + "/frmGeneral.aspx");
                        }
                        else
                        {
                            lblLogin.Text = "Usuario o contraseña invalida.";
                            lblLogin.ForeColor = System.Drawing.Color.Red;
                            Log.EscribeLog("Usuario o contraseña invalida.");
                            Log.EscribeLog("Credenciales intento: " + txtUsuarioUser.Text + " , " + txtContrasenaUser.Text);
                        }
                    }
                    else
                    {
                        lblLogin.Text = "Usuario o contraseña invalida.";
                        lblLogin.ForeColor = System.Drawing.Color.Red;
                        Log.EscribeLog("Usuario o contraseña invalida.");
                        Log.EscribeLog("Credenciales intento: " + txtUsuarioUser.Text + " , " + txtContrasenaUser.Text);
                    }
                }
                else
                {
                    lblLogin.Text = "Verificar el usuario y sitio.";
                    lblLogin.ForeColor = System.Drawing.Color.Red;
                    Log.EscribeLog("Usuario o contraseña invalida.");
                    Log.EscribeLog("Credenciales intento: " + txtUsuarioUser.Text + " , " + txtContrasenaUser.Text);
                }
            }
            catch (Exception eL)
            {
                Log.EscribeLog("Existe un error al iniciar: " + eL.Message);
                lblLogin.Text = "Existe un error." + eL.Message;
                lblLogin.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}