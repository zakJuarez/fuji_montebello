﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FUJI.Feed2.Entidades;
using FUJI.Montebello.AccesoDatos.Extensions;
using FUJI.Montebello.Site.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FUJI.Montebello.Site
{
    public partial class frmDLReport2 : System.Web.UI.Page
    {

        MontebelloService MontebelloDA = new MontebelloService();
        public string URL
        {
            get
            {
                return ConfigurationManager.AppSettings["URL"];
            }
        }

        public string dbLocalServer
        {
            get
            {
                return ConfigurationManager.AppSettings["dbLocalServer"];
            }
        }

        public string dbName
        {
            get
            {
                return ConfigurationManager.AppSettings["dbName"];
            }
        }

        public string dbUser
        {
            get
            {
                return ConfigurationManager.AppSettings["dbUser"];
            }
        }

        public string dbPass
        {
            get
            {
                return ConfigurationManager.AppSettings["dbPass"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserIDMB"] != null && Session["UserIDMB"].ToString() != "" && Session["tbl_CAT_Usuarios"] != null &&
                   Security.ValidateToken(Session["Token"].ToString(), Session["intUsuarioID"].ToString(), Session["UserIDMB"].ToString(), Session["Password"].ToString()))
                {
                    if (!IsPostBack)
                    {
                        clsMensaje response = new clsMensaje();
                        if (Request.QueryString.Count > 0)
                        {
                            lblParanetros.Text = "START";
                            Log.EscribeLog("Inicio");
                            String ID = Security.Decrypt(Request.QueryString["ID"].ToString());
                            lblParanetros.Text += "ID: " + ID;
                            int intEstudioID = ID == null ? 0 : Convert.ToInt32(ID);
                            if (intEstudioID > 0)
                            {
                                response = MontebelloDA.getEstudio(intEstudioID);
                                if (response != null && response.mdlEstudio != null)
                                {
                                    string NumAcc = response.mdlEstudio.vchAccessionNumber == null ? "" : response.mdlEstudio.vchAccessionNumber;
                                    string interpretacion = getInterpretacion(NumAcc);
                                    Log.EscribeLog("Interpretacion: " + interpretacion);    
                                    interpretacion = interpretacion == null ? "" : interpretacion;
                                    interpretacion = interpretacion.Replace("<p>", "");
                                    interpretacion = interpretacion.Replace("&nbsp;", "Chr(9)");
                                    interpretacion = interpretacion.Replace("</p>", System.Environment.NewLine);
                                    string Interpretacion = interpretacion == null ? "" : interpretacion;
                                    ReportDocument crystalReport = new ReportDocument();
                                    Log.EscribeLog("Inicio Carga del Reporte.");
                                    crystalReport.Load(Server.MapPath("~/Data/RepInterpretacion.rpt"));
                                    crystalReport.SetParameterValue("@intEstudioID", intEstudioID);
                                    crystalReport.SetParameterValue("@vchInterpretacion", Interpretacion);
                                    crystalReport.SetDatabaseLogon(dbUser, dbPass, dbLocalServer, dbName);
                                    Log.EscribeLog("Inyeccion de login.");
                                    crystalReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "Interpretacion_" + intEstudioID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf");
                                    Log.EscribeLog("Formacion del reporte.");
                                    Response.Buffer = true;
                                    Response.Clear();
                                    Response.ContentType = "application/pdf";
                                    Response.AddHeader("content-disposition", "attachment; filename=Interpretacion_" + intEstudioID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf");
                                    Response.Flush(); // send it to the client to download
                                    Log.EscribeLog("Carga del Reporte.");
                                }
                                else
                                {
                                    lblTexto.Text = "Error : No es posible leer el estudio.";
                                    Log.EscribeLog("Error: No es posible leer el estudio.");
                                }
                            }
                            else
                            {
                                lblTexto.Text = "Error : No es posible leer el estudio 2.";
                                Log.EscribeLog("Error: No es posible leer el estudio. 2");
                            }
                        }
                        else
                        {
                            lblTexto.Text = "Error : No es posible leer el estudio.3";
                            Log.EscribeLog("Error: No es posible leer el estudio.3");
                        }
                    }
                }
                else
                {
                    Response.Redirect(URL + "/frmLogin.aspx");
                }
            }
            catch (Exception ePL)
            {
                lblTexto.Text = "Error : " + ePL.Message + "<br/>** " + ePL.InnerException;
                Log.EscribeLog("Error al crear el reporte: " + ePL.Message + "   ---Inner: " + ePL.InnerException);
            }
        }

        private string getInterpretacion(string accNum)
        {
            string inter = "";
            try
            {
                string serverpath = Server.MapPath("~/WPFService/Correcto/" + accNum + ".html");
                Log.EscribeLog("Se busca: " + serverpath);
                if (File.Exists(serverpath))
                {
                    //Log.EscribeLog("Texto HTML: " + File.ReadAllText(serverpath));
                    inter = HtmlToPlainText(File.ReadAllText(serverpath));
                    //inter = File.ReadAllText(serverpath);
                }
            }
            catch (Exception egI)
            {
                Log.EscribeLog("Existe un error al obtener la interpretacion: " + egI.Message);
            }
            return inter;
        }

        private static string HtmlToPlainText(string html)
        {
            //try
            //{
            html = html.Replace("<p>INTERPRETACIÓN:</p><p>&nbsp;</p><p>", "");
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            const string lineBreak2 = @"<(p|P)\s{0,1}\/{0,1}>";//matches: <p>,<p/>,<p />,<P>,<P/>,<P />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var lineBreakRegex2 = new Regex(lineBreak2, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            //text = lineBreakRegex.Replace(text, Environment.NewLine);
            text = lineBreakRegex.Replace(text, "Chr(10)"); //Para hacer funcionar el reporte de crystal report
            //Replace <p /> with line breaks
            text = lineBreakRegex2.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);
            //Log.EscribeLog("Texto de interpretación: " + text);
            return text;
            //}
            //catch(Exception eHTML)
            //{
            //    Log.EscribeLog("Existe un error en : HtmlToPlainText : " + eHTML.Message);
            //}
        }
    }
}