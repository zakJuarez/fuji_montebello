﻿using FUJI.Feed2.Entidades;
using FUJI.Montebello.AccesoDatos.Extensions;
using System;
using System.Configuration;
using System.Web.UI;

namespace FUJI.Montebello.Site
{
    public partial class Site : System.Web.UI.MasterPage
    {
        public string URL
        {
            get
            {
                return ConfigurationManager.AppSettings["URL"];
            }
        }

        public static clsUsuario user = new clsUsuario();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["UserIDMB"] != null && Session["UserIDMB"].ToString() != "" && Session["tbl_CAT_Usuarios"] != null &&
                    Security.ValidateToken(Session["Token"].ToString(), Session["intUsuarioID"].ToString(), Session["UserIDMB"].ToString(), Session["Password"].ToString()))
                    {
                        if (Session["tbl_CAT_Usuarios"] != null)
                        {
                            user = (clsUsuario)Session["tbl_CAT_Usuarios"];
                        }
                        hfURL.Value = URL;
                    }
                    lblUser.Text = Session["UserIDMB"].ToString();
                    lblUserTop.Text = user.vchNombre.ToUpper() + " " + user.vchApellido.ToUpper();
                }
            }
            catch (Exception ePL)
            {
                Log.EscribeLog("Existe un error en PageLoad de SiteMaster: " + ePL.Message);
            }
        }


        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Clear();
                Response.Redirect(URL + "/frmLogin.aspx", false);
            }
            catch (Exception ebc)
            {
                throw ebc;
            }
        }

        protected void btnDescargaUtileria_Click(object sender, EventArgs e)
        {
            try
            {
                string urlabrir = "";
                urlabrir = URL + "/frmUtileria.aspx";
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Cerrar", "javascript:Redirecciona('" + urlabrir + "');", true);
            }
            catch(Exception eDescargar)
            {
                Log.EscribeLog("Existe un error al enviar a descargar el archivo: " + eDescargar.Message);
            }
        }

        
    }
}