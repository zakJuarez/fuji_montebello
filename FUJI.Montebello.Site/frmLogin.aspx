﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogin.aspx.cs" Inherits="FUJI.Montebello.Site.frmLogin" %>

<!DOCTYPE html>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>LOG IN Feed2Cloud</title>
    <link rel="stylesheet" href="Content/style.css"/>
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css"/>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <style type="text/css">
        .imagen{
            -webkit-background-size: cover;
           -moz-background-size: cover;
           -o-background-size: cover;
           background-size: cover;
           height: 80%;
           width: 80% ;
           text-align: center;
        }
        .imagenFuji{
            -webkit-background-size: cover;
           -moz-background-size: cover;
           -o-background-size: cover;
           background-size: cover;
           height: 60%;
           width: 60% ;
           text-align: center;
        }
    </style>
</head>
<body>
    <div class="bg-bubbles">
        <div class="wrapper">
            <center>
                <div class="container">
                    <h1>Bienvenido</h1>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <img src="Images/FEED-01.png" class="imagen"/>
                        <form id="form" runat="server" class="form">
                            <div>
                                <asp:TextBox ID="txtUsuarioUser" placeholder="Usuario" runat="server" ValidationGroup="vgLogin" ></asp:TextBox>
		                        <asp:TextBox ID="txtContrasenaUser" TextMode="Password" placeholder="Contraseña" runat="server" ValidationGroup="vgLogin"></asp:TextBox>
		                        <asp:Button type="submit" id="btnLogin" runat="server" Text="Entrar" OnClick="btnLogin_Click" ValidationGroup="vgLogin"></asp:Button>
                                <div>
                                    <asp:RequiredFieldValidator ErrorMessage="Usuario requerido." ForeColor="#61210B" Font-Bold="true" runat="server" ID="rfvUser" ControlToValidate="txtUsuarioUser" ValidationGroup="vgLogin"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ErrorMessage="Contraseña requerida" ForeColor="#61210B" Font-Bold="true" runat="server" ID="rfvPass" ControlToValidate="txtContrasenaUser" ValidationGroup="vgLogin"></asp:RequiredFieldValidator>
                                </div>
                                <br />
                            </div>
                            <img src="Images/Fujifilm_logo.svg.png" class="imagenFuji"/>
                        </form>
                    </div>
                    <!-- footer content -->
                    <footer>
                        <div class="pull-center">
                            <p><%: DateTime.Now.Year %> - Feed2Cloud - Versión 2.0    </p>
                        </div>
                        <div>
                            <br />
                            <br />
                            <asp:Label runat="server" ID="lblLogin" Text=""></asp:Label>
                        </div>
                    </footer>
                    <!-- /footer content -->                    
                </div>
            </center>
        </div>
    </div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
</body>
</html>
