﻿using FUJI.Feed2.Entidades;
using FUJI.Montebello.AccesoDatos.Extensions;
using FUJI.Montebello.Site.Services;
using Microsoft.Reporting.WebForms;
using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace FUJI.Montebello.Site
{
    public partial class frmDLReport : System.Web.UI.Page
    {
        MontebelloService MontebelloDA = new MontebelloService();
        public string URL
        {
            get
            {
                return ConfigurationManager.AppSettings["URL"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserIDMB"] != null && Session["UserIDMB"].ToString() != "" && Session["tbl_CAT_Usuarios"] != null &&
                   Security.ValidateToken(Session["Token"].ToString(), Session["intUsuarioID"].ToString(), Session["UserIDMB"].ToString(), Session["Password"].ToString()))
                {
                    if (!IsPostBack)
                    {
                        clsMensaje response = new clsMensaje();
                        if (Request.QueryString.Count > 0)
                        {
                            lblParanetros.Text = "START";
                            Log.EscribeLog("Inicio");
                            String ID = Security.Decrypt(Request.QueryString["ID"].ToString());
                            lblParanetros.Text += "ID: " + ID;
                            int intEstudioID = ID == null ? 0 : Convert.ToInt32(ID);
                            if (intEstudioID > 0)
                            {
                                response = MontebelloDA.getEstudio(intEstudioID);
                                if (response != null && response.mdlEstudio != null)
                                {
                                    lblParanetros.Text += "Se inicio el reporte";
                                    string NumAcc = response.mdlEstudio.vchAccessionNumber == null ? "" : response.mdlEstudio.vchAccessionNumber;
                                    Log.EscribeLog("Se busca el archivo:" + NumAcc);
                                    string Edad = response.mdlEstudio.vchEdad == null ? "" : response.mdlEstudio.vchEdad;
                                    string FechaEstudio = response.mdlEstudio.datFechaEstudio == null ? (response.mdlEstudio.datFecha == null ? "" : response.mdlEstudio.datFecha.ToString("dd/MM/yyyy HH:mm")) : response.mdlEstudio.datFechaEstudio.ToString("dd/MM/yyyy HH:mm");
                                    string FechaNacimiento = response.mdlEstudio.vchPatientBirthDate == null ? "" : response.mdlEstudio.vchPatientBirthDate;
                                    string Genero = response.mdlEstudio.vchGenero == null ? "" : response.mdlEstudio.vchGenero;
                                    string interpretacion = getInterpretacion(NumAcc);
                                    interpretacion = interpretacion == null ? "" : interpretacion;
                                    interpretacion = interpretacion.Replace("<p>", "");
                                    interpretacion = interpretacion.Replace("&nbsp;", "");
                                    interpretacion = interpretacion.Replace("</p>", System.Environment.NewLine);
                                    string Interpretacion = interpretacion == null ? "" : interpretacion;
                                    string PatientID = response.mdlEstudio.PatientID == null ? "" : response.mdlEstudio.PatientID;
                                    string PatientName = response.mdlEstudio.PatientName == null ? "" : response.mdlEstudio.PatientName;
                                    lblParanetros.Text += "NumAcc: " + NumAcc + " Edad:" + Edad + " FechaEstudio:" + FechaEstudio + " FechaNacimiento:" + FechaNacimiento + " Genero:" + Genero + " PatientID:" + PatientID + " PatientName:" + PatientName;
                                    Log.EscribeLog(lblParanetros.Text);
                                    Log.EscribeLog("Inicio de la carga del Reporte");
                                    // Variables
                                    Warning[] warnings;
                                    string[] streamIds;
                                    string mimeType = string.Empty;
                                    string encoding = string.Empty;
                                    string extension = string.Empty;
                                    ReportViewer viewer = new ReportViewer();
                                    viewer.LocalReport.Refresh();
                                    viewer.ProcessingMode = ProcessingMode.Local;
                                    string pathreport = Server.MapPath("~/Data/Report1.rdlc");
                                    Log.EscribeLog("Ruta del Reporte: " + pathreport);
                                    viewer.LocalReport.ReportPath = pathreport;
                                    ReportParameter p1 = new ReportParameter("NumAcc", NumAcc);
                                    ReportParameter p2 = new ReportParameter("FechaEstudio", FechaEstudio);
                                    ReportParameter p3 = new ReportParameter("PatientID", PatientID);
                                    ReportParameter p4 = new ReportParameter("PatientName", PatientName);
                                    ReportParameter p5 = new ReportParameter("Edad", Edad);
                                    ReportParameter p6 = new ReportParameter("Genero", Genero);
                                    ReportParameter p7 = new ReportParameter("FechaNac", FechaNacimiento);
                                    Log.EscribeLog("Interpretación:" + Interpretacion);
                                    ReportParameter p8 = new ReportParameter("Interpretacion", Interpretacion);
                                    viewer.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7, p8 });
                                    Log.EscribeLog("Parametros: " + NumAcc + "," + FechaEstudio + "," + PatientID + "," + PatientName + "," + Edad + "," + Genero + "," + FechaNacimiento + "," + Interpretacion);
                                    viewer.LocalReport.DisplayName = "Reporte:";
                                    byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                                    var stream = new System.IO.MemoryStream(bytes);
                                    //pdfAtt = new Attachment(stream, "Report_" + NumAcc.NumAcc + ".pdf");


                                    //Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
                                    Response.Buffer = true;
                                    Response.Clear();
                                    Response.ContentType = mimeType;
                                    Response.AddHeader("content-disposition", "attachment; filename=" + NumAcc + "." + extension);
                                    Response.BinaryWrite(bytes); // create the file
                                    Response.Flush(); // send it to the client to download
                                }
                                else
                                {
                                    lblTexto.Text = "Error : No es posible leer el estudio.";
                                }
                            }
                            else
                            {
                                lblTexto.Text = "Error : No es posible leer el estudio.";
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect(URL + "/frmLogin.aspx");
                }
            }
            catch (Exception ePL)
            {
                lblTexto.Text = "Error : " + ePL.Message + "<br/>** " + ePL.InnerException;
            }
        }

        private string getInterpretacion(string accNum)
        {
            string inter = "";
            try
            {
                string serverpath = Server.MapPath("~/WPFService/Correcto/" + accNum + ".html");
                Log.EscribeLog("Se busca: " + serverpath);
                if (File.Exists(serverpath))
                {
                    //Log.EscribeLog("Texto HTML: " + File.ReadAllText(serverpath));
                    inter = HtmlToPlainText( File.ReadAllText(serverpath));
                    //inter = File.ReadAllText(serverpath);
                }
            }
            catch (Exception egI)
            {
                Log.EscribeLog("Existe un error al obtener la interpretacion: " + egI.Message);
            }
            return inter;
        }

        private static string HtmlToPlainText(string html)
        {
            //try
            //{
            html = html.Replace("<p>INTERPRETACIÓN:</p><p>&nbsp;</p><p>", "");
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            const string lineBreak2 = @"<(p|P)\s{0,1}\/{0,1}>";//matches: <p>,<p/>,<p />,<P>,<P/>,<P />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var lineBreakRegex2 = new Regex(lineBreak2, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Replace <p /> with line breaks
            text = lineBreakRegex2.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);
            //Log.EscribeLog("Texto de interpretación: " + text);
            return text;
            //}
            //catch(Exception eHTML)
            //{
            //    Log.EscribeLog("Existe un error en : HtmlToPlainText : " + eHTML.Message);
            //}
        }
    }
}