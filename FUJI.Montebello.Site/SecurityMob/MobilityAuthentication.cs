﻿using FUJI.Montebello.AccesoDatos.Extensions;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace FUJI.Montebello.Site.SecurityMob
{
    public class MobilityAuthentication
    {
        public static string createToken()
        {
            string token = "";
            try
            {
                string username = ConfigurationManager.AppSettings["UserMob"].ToString();
                string shared_secret = ConfigurationManager.AppSettings["PassMob"].ToString();
                DateTime now = DateTime.Now;
                DateTime first = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
                Log.EscribeLog("first: " + first.ToString());
                TimeSpan current_time = (now - first);
                Log.EscribeLog("current_time: " + current_time.TotalSeconds.ToString());
                //string expiry_time = ((long)current_time.TotalSeconds + 3600).ToString();
                string expiry_time = (9999999999 + 3600).ToString();
                Log.EscribeLog("expire_time: " + expiry_time);
                string content = username + ":" + expiry_time;
                string secret_content = content + ":" + shared_secret;
                // Refer to this link https://msdn.microsoft.com/en­us/library/s02tk69a(v=vs.110).aspx
                MD5 md5Hash = MD5.Create();
                string client_hash = GetMd5Hash(md5Hash, secret_content);
                string content_and_hash = content + ":" + client_hash;
                string B64_Content_And_Hash = Convert.ToBase64String(Encoding.UTF8.GetBytes(content_and_hash));
                token = Uri.EscapeDataString(B64_Content_And_Hash);
            }
            catch(Exception eCT)
            {
                Log.EscribeLog("Existe un error al crear el token: " + eCT.Message + " ***" + eCT.InnerException);
            }
            return token;
        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data
            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();

        }

        public static string dameLinkURLMobility(string numeroAcceso)
        {
            string urlacceso = "";
            try
            {
                //Esta es la definición del mensaje JSON que estamos mandando (Se puede mejorar)
                string data = "[\"encid\": \"default\",\"encrypted_params\": [\"AccessionNumber\": \"{0}\"],\"plaintext_params\": [\"action\": \"viewaccession\"]]";

                string URL = ConfigurationManager.AppSettings["urlMobilityService"];
                //URL MOBILITITY
                string k1 = ConfigurationManager.AppSettings["k1"];
                //dmser
                string k2 = ConfigurationManager.AppSettings["k2"];
                //4321

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);

                string autorization = k1 + ":" + k2;
                byte[] binaryAuthorization = System.Text.Encoding.UTF8.GetBytes(autorization);
                autorization = Convert.ToBase64String(binaryAuthorization);
                autorization = Convert.ToString("Basic ") + autorization;

                data = string.Format(data, numeroAcceso);

                data = data.Replace("[", "{").Replace("]", "}");

                // Dim c = JsonConvert.SerializeObject(data)
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.Headers.Add("Authorization", autorization);
                request.ContentLength = data.Length;

                try
                {
                    StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);

                    requestWriter.Write(data);
                    requestWriter.Close();


                    // get the response
                    WebResponse webResponse = request.GetResponse();
                    Stream webStream = webResponse.GetResponseStream();
                    StreamReader responseReader = new StreamReader(webStream);
                    string response = responseReader.ReadToEnd();
                    responseReader.Close();

                    JObject stuff = JObject.Parse(response);
                    urlacceso = stuff["url"].ToString();


                }
                catch (WebException we)
                {
                    Log.EscribeLog("Existe un error al obtener Synapse mobility_: " + we.Message);
                    Log.EscribeLog("Existe un error al obtener Synapse mobility_Inner: " + we.InnerException);
                    throw new System.Exception(we.Message);
                    // no need to do anything special 

                }
                catch (Exception ex)
                {
                    Log.EscribeLog("Existe un error al obtener Synapse mobility_2: " + ex.Message);
                    Log.EscribeLog("Existe un error al obtener Synapse mobility_Inner2: " + ex.InnerException);
                    throw new System.Exception(ex.Message);
                }
            }
            catch (Exception egURL)
            {
                Log.EscribeLog("Error al obtener la url mobility: " + egURL.Message);
            }
            return urlacceso;
        }
    }
}