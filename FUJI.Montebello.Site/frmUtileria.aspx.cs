﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FUJI.Montebello.Site
{
    public partial class frmUtileria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Limpiamos la salida
                Response.Clear();
                // Con esto le decimos al browser que la salida sera descargable
                Response.ContentType = "application/octet-stream";
                // esta linea es opcional, en donde podemos cambiar el nombre del fichero a descargar (para que sea diferente al original)
                Response.AddHeader("Content-Disposition", "attachment; filename=Data/Montebello_permisos.zip");
                // Escribimos el fichero a enviar 
                Response.WriteFile("Data/Montebello_permisos.zip");
                // volcamos el stream 
                Response.Flush();
                // Enviamos todo el encabezado ahora
            }
            catch(Exception ePLDes)
            {
                lblTextoError.Text += "Error : " + ePLDes.Message + " ** " + ePLDes.InnerException;
            }
        }
    }
}