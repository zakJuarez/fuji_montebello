﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmGeneral.aspx.cs" Inherits="FUJI.Montebello.Site.frmGeneral" Culture="es-MX" UICulture="Auto"%>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>

        .ajax__calendar_today
        {
            color:Red;    
        }

        .ajax__calendar_active  
        {
            color: #004080;
            font-weight: bold;
            background-color: #000;
        }

        .cal .ajax__calendar_header
        {
            background-color: Silver;
        }

        .cal .ajax__calendar_container
        {
            background-color: #CEECF5;
        }

        .btn-circle {
          width: 30px;
          height: 30px;
          text-align: center;
          padding: 6px 0;
          font-size: 12px;
          line-height: 1.428571429;
          border-radius: 15px;
        }

    </style>
    <script type="text/javascript">
        function ShowMessage(message, messagetype, idControl) {
            var cssclass;
            switch (messagetype) {
                case 'Success':
                    cssclass = 'alert-success'
                    break;
                case 'Error':
                    cssclass = 'alert-danger'
                    break;
                case 'Warning':
                    cssclass = 'alert-warning'
                    break;
                default:
                    cssclass = 'alert-info'
            }
            var control = "#" + idControl;
            $(control).append('<div id="' + idControl + '" style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');
            $(control).fadeTo(2000, 500).slideUp(500, function () {
                $(control).slideUp(700);
            });
        }

        function Redirecciona(strRuta) {
            var sID = Math.round(Math.random() * 10000000000);
            var winX = screen.availWidth;
            var winY = screen.availHeight;
            sID = "E" + sID;
            window.open(strRuta, sID,
                "menubar=yes,toolbar=yes,location=yes,directories=yes,status=yes,resizable=yes" +
                ",scrollbars=yes,top=0,left=0,screenX=0,screenY=0,Width=" +
                winX + ",Height=" + winY);
        }

        $('input[type=radio]').change(function () {
            alert("test");
        });

        function openPopover() {
            try {
                $('[data-toggle="popover"]').popover();
            }
            catch (ew) {
            }
        }

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover();
        });

        function validateEmail(field) {
            var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}$/;
            return (regex.test(field)) ? true : false;
        }

        function validateMultipleEmailsCommaSeparated(emailcntl, seperator) {
            var value = emailcntl.value;
            if (value != '') {
                var result = value.split(seperator);
                for (var i = 0; i < result.length; i++) {
                    if (result[i].trim() != '') {
                        if (!validateEmail(result[i].trim())) {
                            emailcntl.focus();
                            alert('Por favor revisar, `' + result[i] + '` email no es valido!');
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    </script>
    <asp:HiddenField Value="" ID="idSite" runat="server"  ClientIDMode="Static"/>
    <asp:Timer ID="Timer1" runat="server" Interval="600000" OnTick="Timer1_Tick"></asp:Timer>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="messagealert" id="alert_container">
                    </div>
                    <h2>Estudios</h2>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>                        
                            <button runat="server" id="btnAvisos" type="button" class="btn btn-warning btn-circle" data-toggle="modal" data-target="#exampleModal" title="Avisos" visible="false">
                                <i class="fa fa-exclamation-triangle"></i>
                            </button>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Búsqueda
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-4">
                                            <asp:Label runat="server" ID="lblBusNumEstudio" Text="Num. Estudio" AssociatedControlID="txtBusNumEstudio"></asp:Label>
                                            <asp:TextBox ID="txtBusNumEstudio" runat="server" Text="" CssClass="form-control" OnTextChanged="txtBusNumEstudio_TextChanged" AutoPostBack="true"></asp:TextBox>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-4">
                                            <asp:Label runat="server" ID="lblBusNombre" Text="Nombre" AssociatedControlID="txtBusNombre"></asp:Label>
                                            <asp:TextBox ID="txtBusNombre" runat="server" Text="" CssClass="form-control" OnTextChanged="txtBusNombre_TextChanged"  AutoPostBack="true"></asp:TextBox>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-4">
                                            <asp:Label runat="server" ID="Label1" Text="ID del Paciente" AssociatedControlID="txtBusPatienID"></asp:Label>
                                            <asp:TextBox ID="txtBusPatienID" runat="server" Text="" CssClass="form-control" OnTextChanged="txtBusPatienID_TextChanged"  AutoPostBack="true"></asp:TextBox>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-4">
                                            <asp:Label runat="server" ID="lblModalidad" Text="Modalidad" AssociatedControlID="ddlBusModalidad"></asp:Label>
                                            <asp:DropDownList ID="ddlBusModalidad" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlBusModalidad_SelectedIndexChanged"  AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-3 col-md-3 col-sm-4">
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:50%">
                                                        <asp:Label runat="server" ID="lblBusPrioridad" Text="Fecha Desde" AssociatedControlID="Date1"></asp:Label>
                                                    </td>
                                                    <td style="width:50%">
                                                        <asp:Label runat="server" ID="lblBusEstatus" Text="Fecha Hasta" AssociatedControlID="Date2"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table style="width:100%">
                                                            <tr>
                                                                <td style="width:40%">
                                                                    <asp:TextBox runat="server" ID="Date1" autocomplete="off" CssClass="form-control" Width="100%" Font-Size="Small"/>
                                                                </td>
                                                                <td style="width:10%">
                                                                    <asp:ImageButton ID="imgPopup" ImageUrl="~/Images/ic_action_calendar_month.png"  Width="25px" Height="25px" ImageAlign="Bottom" runat="server" />
                                                                    <ajaxToolkit:CalendarExtender ID="customCalendarExtender" runat="server" TargetControlID="Date1" PopupButtonID="imgPopup"
                                                                    CssClass="cal" Format="dd/MM/yyyy" />
                                                                </td>
                                                                <td style="width:40%">
                                                                    <asp:TextBox runat="server" ID="Date2" autocomplete="off" CssClass="form-control" Width="100%" Font-Size="Small"/>
                                                                </td>
                                                                <td style="width:10%">
                                                                    <asp:ImageButton ID="imgPopup2" ImageUrl="~/Images/ic_action_calendar_month.png" Width="25px" Height="25px" ImageAlign="Bottom" runat="server" />
                                                                    <ajaxToolkit:CalendarExtender ID="customCalendarExtender2" runat="server" TargetControlID="Date2" PopupButtonID="imgPopup2"
                                                                    CssClass="cal" Format="dd/MM/yyyy" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-1 col-md-1 col-sm-4">
                                            <div class="row text-right">
                                                <asp:Button runat="server" ID="btnBusquedaLim" Text="Limpiar" CssClass="btn btnCenter btn-info" OnClick="btnBusquedaLim_Click" />
                                                <asp:Button runat="server" ID="btnBusquedaEst" Text="Buscar/Actualizar" CssClass="btn btnCenter btn-primary" OnClick="btnBusquedaEst_Click"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12  col-sm-12 col-xs-12">
                                <asp:UpdatePanel ID="updGrid" runat="server">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="pnlGrid">
                                        <asp:GridView ID="grvBusqueda" runat="server" AllowPaging="true" CssClass="table table-striped table-bordered"
                                            PageSize="20" AutoGenerateColumns="false" OnRowDataBound="grvBusqueda_RowDataBound" Font-Size="12px"
                                            OnPageIndexChanging="grvBusqueda_PageIndexChanging" DataKeyNames="vchAccessionNumber, intEstudioID, vchCarpetaSyn"
                                            OnRowCommand="grvBusqueda_RowCommand"
                                            EmptyDataText="No hay resultado bajo el criterio de búsqueda.">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%--<div class="row">
                                                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                                                <asp:LinkButton ID="btnSolInt"  CommandName="SolInt" CommandArgument='<%# Bind("intEstudioID") %>' runat="server">
                                                                    <asp:Image ID="imgSolInt" runat="server" ImageUrl="~/Images/ic_action_document.png" Height="25px" Width="25px" ToolTip="Solicitar Interpretación"/>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                                                <asp:LinkButton ID="btnSyn"  CommandName="ShowSyn" CommandArgument='<%# Bind("vchAccessionNumber") %>' runat="server">
                                                                    <asp:Image ID="imgSyn" runat="server" ImageUrl="~/Images/Synapse.png" Height="25px" Width="25px" ToolTip="Synapse"/>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                                                <asp:LinkButton ID="btnMob"  CommandName="ShowMob" CommandArgument='<%# Bind("vchAccessionNumber") %>' runat="server">
                                                                    <asp:Image ID="imgMob" runat="server" ImageUrl="~/Images/ic_action_eye_open.png" Height="25px" Width="25px" ToolTip="Mobility"/>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                                                <asp:LinkButton ID="btnCorreo"  CommandName="SendEmail" CommandArgument='<%# Bind("intEstudioID") %>' runat="server">
                                                                    <asp:Image ID="imgEmail" runat="server" ImageUrl="~/Images/ic_action_mail.png" Height="25px" Width="25px" ToolTip="Enviar Correo"/>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                                                <asp:LinkButton ID="btnReport"  CommandName="Report" CommandArgument='<%# Bind("intEstudioID") %>' runat="server">
                                                                    <asp:Image ID="imgReport" runat="server" ImageUrl="~/Images/ic_action_document.png" Height="25px" Width="25px" ToolTip="Reporte"/>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                                            </div>
                                                        </div>--%>
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                                <asp:LinkButton ID="btnSyn"  CommandName="ShowSyn" CommandArgument='<%# Bind("vchAccessionNumber") %>' runat="server">
                                                                    <asp:Image ID="imgSyn" runat="server" ImageUrl="~/Images/Synapse.png" Height="25px" Width="25px" ToolTip="Synapse"/>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                                <asp:LinkButton ID="btnMob"  CommandName="ShowMob" CommandArgument='<%# Bind("vchAccessionNumber") %>' runat="server">
                                                                    <asp:Image ID="imgMob" runat="server" ImageUrl="~/Images/ic_action_eye_open.png" Height="25px" Width="25px" ToolTip="Mobility"/>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                                <asp:LinkButton ID="btnCorreo"  CommandName="SendEmail" CommandArgument='<%# Bind("intEstudioID") %>' runat="server">
                                                                    <asp:Image ID="imgEmail" runat="server" ImageUrl="~/Images/ic_action_mail.png" Height="25px" Width="25px" ToolTip="Enviar Correo"/>
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                                <asp:LinkButton ID="btnReport"  CommandName="Report" CommandArgument='<%# Bind("intEstudioID") %>' runat="server">
                                                                    <asp:Image ID="imgReport" runat="server" ImageUrl="~/Images/ic_action_document.png" Height="25px" Width="25px" ToolTip="Reporte"/>
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="vchAccessionNumber"  HeaderText="Num. de Estudio" ReadOnly="true" />
                                                <asp:BoundField DataField="vchModalidadID" HeaderText="Modalidades" ReadOnly="true" HeaderStyle-CssClass="visible-lg" ItemStyle-CssClass="visible-lg" />
                                                <asp:BoundField DataField="PatientID" HeaderText="Folio Paciente" ReadOnly="true" />
                                                <asp:BoundField DataField="PatientName" HeaderText="Nombre" ReadOnly="true" />
                                                <asp:BoundField DataField="vchPatientBirthDate" HeaderText="Fecha Nacimiento" ReadOnly="true" ItemStyle-CssClass="hidden-xs" HeaderStyle-CssClass="hidden-xs" />
                                                <asp:BoundField DataField="vchEdad" HeaderText="Edad" ReadOnly="true" HeaderStyle-CssClass="hidden-md hidden-xs" ItemStyle-CssClass="hidden-md hidden-xs"/>
                                                <asp:BoundField DataField="vchGenero" HeaderText="Genero" ReadOnly="true" HeaderStyle-CssClass="hidden-md hidden-xs" ItemStyle-CssClass="hidden-md hidden-xs"/>
                                                <asp:BoundField DataField="datFechaEstudio" HeaderText="Fecha Estudio" ReadOnly="true" HeaderStyle-CssClass="hidden-md hidden-xs" ItemStyle-CssClass="hidden-md hidden-xs"/>
                                                <asp:BoundField DataField="datFecha" HeaderText="Fecha" ReadOnly="true" HeaderStyle-CssClass="hidden-md hidden-xs" ItemStyle-CssClass="hidden-md hidden-xs"/>
                                                
                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="lblTemplate" runat="server" Text="Muestra Filas: " CssClass="Label" />
                                                <asp:DropDownList ID="ddlBandeja" runat="server" AutoPostBack="true" CausesValidation="false"
                                                    Enabled="true" OnSelectedIndexChanged="ddlBandeja_SelectedIndexChanged">
                                                        <asp:ListItem Value="10" />
                                                        <asp:ListItem Value="15" />
                                                        <asp:ListItem Value="20" />
                                                </asp:DropDownList>
                                                &nbsp;Página
                                                <asp:TextBox ID="txtBandeja" runat="server" AutoPostBack="true" OnTextChanged="txtBandeja_TextChanged"
                                                    Width="40" MaxLength="10" />
                                                de
                                                <asp:Label ID="lblBandejaTotal" runat="server" />
                                                &nbsp;
                                                <asp:Button ID="btnBandeja_I" runat="server" CommandName="Page" CausesValidation="false"
                                                    ToolTip="Página Anterior" CommandArgument="Prev" CssClass="previous" />
                                                <asp:Button ID="btnBandeja_II" runat="server" CommandName="Page" CausesValidation="false"
                                                    ToolTip="Página Siguiente" CommandArgument="Next" CssClass="next" />
                                            </PagerTemplate>
                                            <HeaderStyle CssClass="headerstyle" />
                                            <FooterStyle CssClass="text-center" />
                                            <PagerStyle CssClass="text-center" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Email -->
    <center>
    <div class="modal fade" id="modalEmail" tabindex="-1" role="dialog" >
      <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:45%"> 
          <asp:UpdatePanel ID="updEmail" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="messagealert" id="alertEmail_container">
                        </div>
                        <div class="panel-heading">
                            Enviar por correo <asp:Label runat="server" ID="lblEstudio" Text="" ></asp:Label><asp:Label runat="server" ID="lblIntEstudio" Text="" ></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <asp:CheckBox CssClass="btn-info btn-sm" Text="Estudio" runat="server" id="chkEstudio" ></asp:CheckBox>
                                <asp:CheckBox CssClass="btn-warning btn-sm" Text="Reporte"   runat="server" id="chkReporte"  ></asp:CheckBox>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblEmails" runat="server" Text="Email(s):" ForeColor="#00897B" ></asp:Label>
                            </div>
                            <div  class="row">
                                <asp:TextBox runat="server" Text="" ID="txtEmails" CssClass="form-control" onblur="validateMultipleEmailsCommaSeparated(this,';');" Width="80%"></asp:TextBox>
                            </div>
                            <div class="row">
                                <asp:Label ID="lblMensaje" runat="server" Text="Asunto:" ForeColor="#00897B" ></asp:Label>
                            </div>
                            <div class="row">
                                <asp:TextBox runat="server" ID="txtAsunto"  Text="" CssClass="form-control" Width="80%"/>
                            </div>
                            <div class="clearfix"></div>
                            <br />
                            <div class="row">
                                <asp:Button ID="btnCancelarCorreo" runat="server" Text="Cancelar" ToolTip="" CssClass="btn btn-danger btn-sm" OnClick="btnCancelarCorreo_Click" />
                                <asp:Button ID="btnGuardarCorreo" runat="server" Text="Enviar" CssClass="btn btn-default btn-sm" style="background-color: #08A537; border-color: #01992E; color:white;" OnClick="btnGuardarCorreo_Click"/>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    </center>
    <!-- /Email -->


   <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h2 class="modal-title" id="exampleModalLabel">AVISOS</h2>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <asp:Label ID="lblAvisos" CssClass="col-lg-12 col-sm-12 col-md-12" Enabled="false" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button runat="server" ID="btnCloseexampleModal" class="btn btn-primary" OnClick="btnCloseexampleModal_Click" Text="Aceptar"></asp:Button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
    </div>
    
</asp:Content>
