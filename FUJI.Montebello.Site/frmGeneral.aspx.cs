﻿using FUJI.Feed2.Entidades;
using FUJI.Montebello.AccesoDatos.DataAccess;
using FUJI.Montebello.AccesoDatos.Extensions;
using FUJI.Montebello.Site.SecurityMob;
using FUJI.Montebello.Site.Services;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FUJI.Montebello.Site
{
    public partial class frmGeneral : System.Web.UI.Page
    {
        MontebelloService MontebelloDA = new MontebelloService();
        public string URL
        {
            get
            {
                return ConfigurationManager.AppSettings["URL"];
            }
        }

        public int id_sitio = 0;

        public string URLSyn
        {
            get
            {
                return ConfigurationManager.AppSettings["URLSyn"];
            }
        }
        public string URLSynVNA
        {
            get
            {
                return ConfigurationManager.AppSettings["URLSynVNA"];
            }
        }
        public string URLSynHFJS
        {
            get
            {
                return ConfigurationManager.AppSettings["URLSynHFJS"];
            }
        }
        public string URLSynCTYO
        {
            get
            {
                return ConfigurationManager.AppSettings["URLSynCTYO"];
            }
        }
        public string URLMob
        {
            get
            {
                return ConfigurationManager.AppSettings["URLMob"];
            }
        }
        public static List<clsEstudio> lstCompleto = new List<clsEstudio>();
        public static clsUsuario mdlUsuario = new clsUsuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserIDMB"] != null && Session["UserIDMB"].ToString() != "" && Session["tbl_CAT_Usuarios"] != null &&
                Security.ValidateToken(Session["Token"].ToString(), Session["intUsuarioID"].ToString(), Session["UserIDMB"].ToString(), Session["Password"].ToString()))
                {
                    if (!IsPostBack)
                    {
                        Log.EscribeLog("Inicia frmGeneral");
                        mdlUsuario = (clsUsuario)Session["tbl_CAT_Usuarios"];
                        id_sitio = (int)Session["id_sitio"];
                        idSite.Value = id_sitio.ToString();
                        customCalendarExtender.StartDate = Convert.ToDateTime("01/01/2017");
                        //customCalendarExtender.SelectedDate = Convert.ToDateTime("01/01/2017");
                        //Date1.Text = "01/01/2017";
                        //customCalendarExtender2.EndDate = DateTime.Today;
                        //Date2.Text = DateTime.Today.ToString("dd/MM/yyyy");
                        Log.EscribeLog("Inicia frmGeneral");
                        cargaCatalogo();
                        Log.EscribeLog("Termina frmGeneral");

                        Log.EscribeLog("Inicia Cargar Estudio");
                        cargarEstudios();
                        Log.EscribeLog("Termina Cargar Estudio");

                        Date1.Attributes.Add("readonly", "readonly");
                        Date2.Attributes.Add("readonly", "readonly");
                    }
                }
                else
                {
                    Response.Redirect(URL + "/frmLogin.aspx");
                }
            }
            catch (Exception ePL)
            {
                Log.EscribeLog("Existe un error al cargar la página general: " + ePL.Message);
            }
        }

        private void cargarAviso(int id_sitio)
        {
            try
            {
                tbl_MST_Avisos avisos = MontebelloDA.obtenerAvisos(id_sitio);
                if(avisos != null && avisos.intAvisoID > 0)
                {
                    Session["AvisoMDL"] = avisos;
                    lblAvisos.Text = avisos.vchAviso;
                    btnAvisos.Visible = true;
                }
                else
                {
                    lblAvisos.Text = "";
                    btnAvisos.Visible = false;
                }
            }
            catch (Exception ecAviso)
            {
                Log.EscribeLog("Error al cargar avisos: " + ecAviso.Message);
            }
        }

        private void cargarEstudios()
        {
            try
            {
                cargarAviso(id_sitio);
                clsMensaje response = new clsMensaje();
                clsEstudio mdlBusqueda = new clsEstudio();
                mdlBusqueda = obtenerBusqueda();
                if (mdlBusqueda != null)
                {
                    int idSiteID = id_sitio == Convert.ToInt32(idSite.Value) ? id_sitio : Convert.ToInt32(idSite.Value);
                    Log.EscribeLog("Id_sitio: " + idSiteID);
                    Log.EscribeLog("usuario: " + mdlUsuario.vchUsuario);
                    Log.EscribeLog("usuario_id_sitio: " + mdlUsuario.id_Sitio);
                    response = MontebelloDA.getListEstudiosParam(idSiteID, mdlBusqueda.intModalidadID, mdlBusqueda.datFechaIni, mdlBusqueda.datFechaFin, mdlBusqueda.vchAccessionNumber, mdlBusqueda.PatientID, mdlBusqueda.PatientName);
                    if (response != null && response._lstEst != null)
                    {
                        if (response._lstEst.Count > 0)
                        {
                            Log.EscribeLog("Estudios: " + response._lstEst.Count.ToString());
                            List<clsEstudio> lst = new List<clsEstudio>();
                            lst = response._lstEst;
                            //Log.EscribeLog("1");
                            //if (mdlBusqueda.vchAccessionNumber != "")
                            //{
                            //    lst = lst.Where(x => x.vchAccessionNumber.ToUpper().Contains(mdlBusqueda.vchAccessionNumber.ToUpper())).ToList();
                            //}
                            //Log.EscribeLog("2");
                            //if (mdlBusqueda.PatientName != "")
                            //{
                            //    lst = lst.Where(x => x.PatientName.ToUpper().Contains(mdlBusqueda.PatientName.ToUpper())).ToList();
                            //}
                            //Log.EscribeLog("3");
                            //if (mdlBusqueda.PatientID != "")
                            //{
                            //    lst = lst.Where(x => x.PatientID.ToUpper().Contains(mdlBusqueda.PatientID.ToUpper())).ToList();
                            //}
                            Log.EscribeLog("4");
                            if (mdlBusqueda.intModalidadID > 0)
                            {
                                lst = lst.Where(x => x.intModalidadID == mdlBusqueda.intModalidadID).ToList();
                            }

                            //if (mdlBusqueda.datFechaIni != DateTime.MinValue && mdlBusqueda.datFechaFin != DateTime.MinValue)
                            //{
                            //    if (mdlBusqueda.datFechaIni <= mdlBusqueda.datFechaFin)
                            //    {
                            //        lst = lst.Where(x => x.datFecha.Date <= mdlBusqueda.datFechaFin.Date && x.datFecha.Date >= mdlBusqueda.datFechaIni.Date).ToList();
                            //    }
                            //}
                            Log.EscribeLog("5");
                            lstCompleto = lst.OrderByDescending(x => x.datFecha).ToList();
                            grvBusqueda.DataSource = lstCompleto;
                        }
                    }
                    else
                    {
                        Log.EscribeLog("Sin resultados de estudios.");
                        grvBusqueda.DataSource = null;
                    }
                    Log.EscribeLog("6");
                    grvBusqueda.DataBind();
                }
                else
                {
                    Log.EscribeLog("Verificar los datos de búsqueda.");
                    ShowMessage("Verificar los datos de búsqueda.", MessageType.Warning, "alert_container");
                }
            }
            catch (Exception ecE)
            {
                Log.EscribeLog("Existe un error al cargar estudios: " + ecE.Message);
            }
        }

        private clsEstudio obtenerBusqueda()
        {
            clsEstudio mdlBus = new clsEstudio();
            try
            {
                bool valido = false;
                bool valido2 = false;
                DateTime dateValueIni;
                DateTime dateValueFin;
                if (DateTime.TryParse(Date1.Text, out dateValueIni))
                {
                    valido = true;
                }
                if (DateTime.TryParse(Date2.Text, out dateValueFin))
                {
                    valido2 = true;
                }
                mdlBus.id_Sitio = (int)mdlUsuario.id_Sitio;
                mdlBus.vchAccessionNumber = txtBusNumEstudio.Text;
                mdlBus.PatientName = txtBusNombre.Text;
                mdlBus.PatientID = txtBusPatienID.Text;
                mdlBus.intModalidadID = Convert.ToInt32(ddlBusModalidad.SelectedValue);
                if (valido && valido2)
                {
                    mdlBus.datFechaIni = Date1.Text == "" ? DateTime.MinValue : dateValueIni;
                    mdlBus.datFechaFin = Date2.Text == "" ? DateTime.MinValue : dateValueFin;
                }
                else
                {
                    //customCalendarExtender.SelectedDate = Convert.ToDateTime("01/01/2017");
                    Date1.Text = "01/01/2017";
                    //customCalendarExtender2.SelectedDate = DateTime.Now;
                    Date2.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    mdlBus.datFechaIni = Date1.Text == "" ? DateTime.MinValue : Convert.ToDateTime(Date1.Text);
                    mdlBus.datFechaFin = Date2.Text == "" ? DateTime.MinValue : Convert.ToDateTime(Date2.Text);
                }
            }
            catch (Exception eOB)
            {
                Log.EscribeLog("Existe un error al obtener los datos de la búsqueda : " + eOB.Message);
            }
            return mdlBus;
        }

        private void cargaCatalogo()
        {
            try
            {
                ddlBusModalidad.Items.Clear();
                List<tbl_CAT_Modalidad> lstCat = new List<tbl_CAT_Modalidad>();
                lstCat = MontebelloDA.getCatModalidad();
                ddlBusModalidad.DataSource = lstCat;
                ddlBusModalidad.DataTextField = "vchModalidadDesc";
                ddlBusModalidad.DataValueField = "intModalidadID";
                ddlBusModalidad.DataBind();
                ddlBusModalidad.Items.Insert(0, new ListItem("Todas...", "0"));
            }
            catch (Exception ecc)
            {
                Log.EscribeLog("Existe un error al cargar los catalogos: " + ecc.Message);
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                cargarEstudios();
            }
            catch (Exception eTimer)
            {
                Log.EscribeLog("Existe un error al actualizar el timmer: " + eTimer.Message);
            }
        }

        protected void grvBusqueda_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex >= 0)
                {
                    this.grvBusqueda.PageIndex = e.NewPageIndex;
                    cargarEstudios();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Existe un error: " + ex.Message, MessageType.Error, "alert_container");
            }
        }

        protected void grvBusqueda_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    Label lblTotalNumDePaginas = (Label)e.Row.FindControl("lblBandejaTotal");
                    lblTotalNumDePaginas.Text = grvBusqueda.PageCount.ToString();

                    TextBox txtIrAlaPagina = (TextBox)e.Row.FindControl("txtBandeja");
                    txtIrAlaPagina.Text = (grvBusqueda.PageIndex + 1).ToString();

                    DropDownList ddlTamPagina = (DropDownList)e.Row.FindControl("ddlBandeja");
                    ddlTamPagina.SelectedValue = grvBusqueda.PageSize.ToString();
                }

                if (e.Row.RowType != DataControlRowType.DataRow)
                {
                    return;
                }

                clsEstudio _mdl = new clsEstudio();
                _mdl = e.Row.DataItem as clsEstudio;
                Log.EscribeLog("AccNum: " + _mdl.vchAccessionNumber + ".");
                LinkButton btnReport = (LinkButton)e.Row.FindControl("btnReport");
                //LinkButton btnSolInt = (LinkButton)e.Row.FindControl("btnSolInt");
                if (_mdl.intEstatusID == 4)
                {
                    btnReport.Visible = true;
                    //btnSolInt.Visible = false;
                }
                else
                {
                    btnReport.Visible = false;
                    //btnSolInt.Visible = true;
                    //Image imgSolInt = (Image)e.Row.FindControl("imgSolInt");
                    //imgSolInt.Attributes.Add("onclick", "javascript:return confirm('¿Desea realizar la solicitud de interpretación del estudio?');");
                    //if ((bool)_mdl.bitSolInterpretacion)
                    //{
                    //    imgSolInt.ImageUrl = @"~/Images/conSolicitud.png";
                    //    imgSolInt.ToolTip = "Ya tiene solicitud de interpretación";
                    //    btnSolInt.Enabled = false;
                    //}
                    //else
                    //{
                    //    imgSolInt.ImageUrl = @"~/Images/solicitud.png";
                    //    imgSolInt.ToolTip = "Solicitar interpretación";
                    //    btnSolInt.Enabled = true;
                    //}
                }
            }
            catch (Exception egrdb)
            {
                throw new Exception(egrdb.Message);
            }
        }

        protected void grvBusqueda_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string AccNum = "";
                string urlabrir = "";
                int intEstudioID = 0;
                switch (e.CommandName)
                {
                    case "ShowSyn":
                        Control ctl = e.CommandSource as Control;
                        GridViewRow CurrentRow = ctl.NamingContainer as GridViewRow;
                        string Carpeta = grvBusqueda.DataKeys[CurrentRow.RowIndex].Values["vchCarpetaSyn"].ToString();
                        if (e.CommandArgument.ToString().Length > 16)
                            AccNum = e.CommandArgument.ToString().Substring(0, 16);
                        else
                            AccNum = e.CommandArgument.ToString();
                        if (Carpeta != null && Carpeta != "")
                        {
                            urlabrir = URLSynVNA.Replace("CARPETA", Carpeta) + AccNum;
                        }
                        else
                        {
                            urlabrir = URLSyn + AccNum;
                        }
                        Log.EscribeLog("URL Synapse: " + urlabrir);
                        //urlabrir = URLSyn + AccNum;
                        //Log.EscribeLog("Usuario: " + mdlUsuario.id_Sitio.ToString());
                        //if (mdlUsuario.id_Sitio == 10)
                        //{
                        //    urlabrir = URLSynHFJS + AccNum;
                        //}
                        //if (mdlUsuario.id_Sitio == 12)
                        //{
                        //    urlabrir = URLSynCTYO + AccNum;
                        //}
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Cerrar", "javascript:Redirecciona('" + urlabrir + "');", true);
                        break;
                    case "ShowMob":
                        AccNum = e.CommandArgument.ToString();
                        string url = "";
                        url = MobilityAuthentication.dameLinkURLMobility(AccNum.ToUpper());
                        //urlabrir = URLMob.Replace("ACCNUM", AccNum.ToUpper());
                        //string urlMoby = "";
                        //urlMoby = MobilityAuthentication.dameLinkURLMobility(AccNum.ToUpper());
                        string token = MobilityAuthentication.createToken();
                        string autenURL = ConfigurationManager.AppSettings["complementoURL"].ToString().Replace("TOKEN", token);
                        string urlabrir1 = "";
                        urlabrir1 = url + autenURL;
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Cerrar", "javascript:Redirecciona('" + (urlabrir1 != "" ? urlabrir1 : url) + "');", true);
                        break;
                    case "SendEmail":
                        intEstudioID = Convert.ToInt32(e.CommandArgument.ToString());
                        lblIntEstudio.Text = intEstudioID.ToString();
                        clsEstudio mdlEst = new clsEstudio();
                        if (lstCompleto != null && lstCompleto.Count > 0)
                        {
                            if (lstCompleto.Any(x => x.intEstudioID == intEstudioID))
                            {
                                mdlEst = lstCompleto.First(x => x.intEstudioID == intEstudioID);
                                AccNum = mdlEst.vchAccessionNumber;
                                lblEstudio.Text = AccNum;
                                chkEstudio.Checked = true;
                                if (mdlEst.intEstatusID == 4)
                                {
                                    chkReporte.Visible = true;
                                }
                                else
                                {
                                    chkReporte.Visible = false;
                                }
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalEmail", "$('#modalEmail').modal();", true);
                            }
                            else
                            {
                                ShowMessage("Actualizar la información.", MessageType.Info, "alert_container");
                            }
                        }
                        else
                        {
                            ShowMessage("Actualizar la información.", MessageType.Info, "alert_container");
                        }

                        break;
                    case "Report":
                        intEstudioID = Convert.ToInt32(e.CommandArgument.ToString());
                        clsEstudio mdlEstRe = new clsEstudio();
                        if (lstCompleto != null && lstCompleto.Count > 0)
                        {
                            if (lstCompleto.Any(x => x.intEstudioID == intEstudioID))
                            {
                                mdlEstRe = lstCompleto.First(x => x.intEstudioID == intEstudioID);
                                if (mdlEstRe != null)
                                {
                                    descargaPDF(intEstudioID);
                                }
                                else
                                {
                                    ShowMessage("Verificar la información", MessageType.Error, "alert_container");
                                }
                            }
                            else
                            {
                                ShowMessage("Actualizar la información.", MessageType.Info, "alert_container");
                            }
                        }
                        else
                        {
                            ShowMessage("Actualizar la información.", MessageType.Info, "alert_container");
                        }
                        break;
                    case "SolInt":
                        intEstudioID = Convert.ToInt32(e.CommandArgument.ToString());
                        clsEstudio mdlSolInt = new clsEstudio();
                        if (lstCompleto != null && lstCompleto.Count > 0)
                        {
                            if (lstCompleto.Any(x => x.intEstudioID == intEstudioID))
                            {
                                mdlEstRe = lstCompleto.First(x => x.intEstudioID == intEstudioID);
                                if (mdlEstRe != null)
                                {
                                    clsMensaje response = new clsMensaje();
                                    response = MontebelloDA.setSolicitarInterpretacion(intEstudioID);
                                    if (response != null)
                                    {
                                        if (response.valido)
                                        {
                                            cargarEstudios();
                                            ShowMessage("Se envió la solicitud", MessageType.Success, "alert_container");
                                        }
                                        else
                                        {
                                            ShowMessage("Existe un error: " + response.vchMensaje, MessageType.Error, "alert_container");
                                        }
                                    }
                                    else
                                    {
                                        ShowMessage("Favor de verificar la información-", MessageType.Error, "alert_container");
                                    }
                                }
                                else
                                {
                                    ShowMessage("Verificar la información", MessageType.Error, "alert_container");
                                }
                            }
                            else
                            {
                                ShowMessage("Actualizar la información.", MessageType.Info, "alert_container");
                            }
                        }
                        else
                        {
                            ShowMessage("Actualizar la información.", MessageType.Info, "alert_container");
                        }
                        break;
                    case "Aviso":
                        break;
                }
            }
            catch (Exception egrRc)
            {
                Log.EscribeLog("Existe un error en grvBusqueda_RowCommand:" + egrRc.Message + " ***" + egrRc.InnerException);
                ShowMessage("Existe un error en grvBusqueda_RowCommand: " + egrRc.Message, MessageType.Error, "alert_container");
            }
        }

        private void descargaPDF(int intEstudioID)
        {
            try
            {
                string urlabrir = "";
                string id = Security.Encrypt(intEstudioID.ToString());
                //urlabrir = URL + "/frmDLReport.aspx?ID=" + id;
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Cerrar", "javascript:Redirecciona('" + urlabrir + "');", true);
                urlabrir = URL + "/frmDLReport2.aspx?ID=" + id;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Cerrar", "javascript:Redirecciona('" + urlabrir + "');", true);
            }
            catch (Exception edPDF)
            {
                Log.EscribeLog("Existe un error al descargar el pdf" + edPDF.Message);
            }
        }

        protected void ddlBandeja_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList dropDownList = (DropDownList)sender;
                if (int.Parse(dropDownList.SelectedValue) != 0)
                {
                    this.grvBusqueda.AllowPaging = true;
                    this.grvBusqueda.PageSize = int.Parse(dropDownList.SelectedValue);
                }
                else
                    this.grvBusqueda.AllowPaging = false;
                this.cargarEstudios();
            }
            catch (Exception eddS)
            {
                ShowMessage("Existe un error: " + eddS.Message, MessageType.Error, "alert_container");
            }
        }

        protected void txtBandeja_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox txtBandejaAvaluosGoToPage = (TextBox)sender;
                int numeroPagina;
                if (int.TryParse(txtBandejaAvaluosGoToPage.Text.Trim(), out numeroPagina))
                    this.grvBusqueda.PageIndex = numeroPagina - 1;
                else
                    this.grvBusqueda.PageIndex = 0;
                this.cargarEstudios();
            }
            catch (Exception ex)
            {
                ShowMessage("Existe un error: " + ex.Message, MessageType.Error, "alert_container");
            }
        }

        protected void btnBusquedaEst_Click(object sender, EventArgs e)
        {
            try
            {
                cargarEstudios();
            }
            catch (Exception eBS)
            {
                ShowMessage("Existe un error al realizar la búsqueda: " + eBS.Message, MessageType.Error, "alert_container");
            }
        }

        public enum MessageType { Success, Error, Info, Warning };

        protected void ShowMessage(string Message, MessageType type, String container)
        {
            try
            {
                Message = Message.Replace("'", "");
                ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "','" + container + "');", true);
            }
            catch (Exception eSM)
            {
                throw eSM;
            }
        }

        protected void txtBusNumEstudio_TextChanged(object sender, EventArgs e)
        {
            try
            {
                cargarEstudios();
            }
            catch (Exception eBS)
            {
                ShowMessage("Existe un error al realizar la búsqueda: " + eBS.Message, MessageType.Error, "alert_container");
            }
        }

        protected void txtBusNombre_TextChanged(object sender, EventArgs e)
        {
            try
            {
                cargarEstudios();
            }
            catch (Exception eBS)
            {
                ShowMessage("Existe un error al realizar la búsqueda: " + eBS.Message, MessageType.Error, "alert_container");
            }
        }

        protected void txtBusPatienID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                cargarEstudios();
            }
            catch (Exception eBS)
            {
                ShowMessage("Existe un error al realizar la búsqueda: " + eBS.Message, MessageType.Error, "alert_container");
            }
        }

        protected void ddlBusModalidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cargarEstudios();
            }
            catch (Exception eBS)
            {
                ShowMessage("Existe un error al realizar la búsqueda: " + eBS.Message, MessageType.Error, "alert_container");
            }
        }

        protected void Date1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime dateValueIni;
                DateTime dateValueFin;
                if (DateTime.TryParse(Date1.Text, out dateValueIni))
                {
                    if (DateTime.TryParse(Date2.Text, out dateValueFin))
                    {
                        if (dateValueIni > dateValueFin)
                        {
                            //Date1.Text = DateTime.Today.ToString("dd/MM/yyyy");
                            customCalendarExtender.SelectedDate = DateTime.Today;
                            //Date2.Text = DateTime.Today.ToString("dd/MM/yyyy");
                            customCalendarExtender2.SelectedDate = DateTime.Today;
                        }
                        //else
                        //{
                        //    Date1.Text = Convert.ToDateTime(customCalendarExtender.SelectedDate).ToString("dd/MM/yyyy");
                        //    customCalendarExtender.SelectedDate = DateTime.Today;
                        //    Date2.Text = Convert.ToDateTime(customCalendarExtender2.SelectedDate).ToString("dd/MM/yyyy");
                        //    customCalendarExtender2.SelectedDate = DateTime.Today;
                        //}
                    }
                    else
                    {
                        //Date2.Text = DateTime.Today.ToString("dd/MM/yyyy");
                        customCalendarExtender2.SelectedDate = DateTime.Today;
                    }
                }
                else
                {
                    //Date1.Text = DateTime.Today.ToString("dd/MM/yyyy");
                    customCalendarExtender.SelectedDate = DateTime.Today;
                }
                //cargarEstudios();
            }
            catch (Exception eBS)
            {
                ShowMessage("Existe un error al realizar la búsqueda: " + eBS.Message, MessageType.Error, "alert_container");
            }
        }

        protected void Date2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime dateValueIni;
                DateTime dateValueFin;
                if (DateTime.TryParse(Date1.Text, out dateValueIni))
                {
                    if (DateTime.TryParse(Date2.Text, out dateValueFin))
                    {
                        if (dateValueIni > dateValueFin)
                        {
                            //Date1.Text = DateTime.Today.ToString("dd/MM/yyyy");
                            customCalendarExtender.SelectedDate = DateTime.Today;
                            //Date2.Text = DateTime.Today.ToString("dd/MM/yyyy");
                            customCalendarExtender2.SelectedDate = DateTime.Today;
                        }
                        //else
                        //{
                        //    Date1.Text = Convert.ToDateTime(customCalendarExtender.SelectedDate).ToString("dd/MM/yyyy");
                        //    customCalendarExtender.SelectedDate = DateTime.Today;
                        //    Date2.Text = Convert.ToDateTime(customCalendarExtender2.SelectedDate).ToString("dd/MM/yyyy");
                        //    customCalendarExtender2.SelectedDate = DateTime.Today;
                        //}
                    }
                    else
                    {
                        //Date2.Text = DateTime.Today.ToString("dd/MM/yyyy");
                        customCalendarExtender2.SelectedDate = DateTime.Today;
                    }
                }
                else
                {
                    //Date1.Text = DateTime.Today.ToString("dd/MM/yyyy");
                    customCalendarExtender.SelectedDate = DateTime.Today;
                }
                //cargarEstudios();
            }
            catch (Exception eBS)
            {
                ShowMessage("Existe un error al realizar la búsqueda: " + eBS.Message, MessageType.Error, "alert_container");
            }
        }

        protected void btnBusquedaLim_Click(object sender, EventArgs e)
        {
            try
            {
                limpiarControles();
                cargarEstudios();
            }
            catch (Exception eBS)
            {
                ShowMessage("Existe un error al realizar la búsqueda: " + eBS.Message, MessageType.Error, "alert_container");
            }
        }

        private void limpiarControles()
        {
            try
            {
                txtBusNumEstudio.Text = "";
                txtBusNombre.Text = "";
                txtBusPatienID.Text = "";
                ddlBusModalidad.SelectedValue = "0";
                //customCalendarExtender.SelectedDate = Convert.ToDateTime("01/01/2017");
                Date1.Text = "01/01/2017";
                //customCalendarExtender2.SelectedDate = DateTime.Now;
                Date2.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
            catch (Exception eLC)
            {
                Log.EscribeLog("Error al limpiar los controles: " + eLC.Message);
            }
        }

        protected void btnCancelarCorreo_Click(object sender, EventArgs e)
        {
            try
            {
                limpiarModalEmail();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalEmail", "$('#modalEmail').modal('hide');", true);
            }
            catch (Exception eCA)
            {
                ShowMessage("Existe un error: " + eCA.Message, MessageType.Error, "alert_container");
            }
        }

        private void limpiarModalEmail()
        {
            try
            {
                lblIntEstudio.Text = "";
                lblEstudio.Text = "";
                chkEstudio.Checked = true;
                chkReporte.Checked = false;
                txtEmails.Text = "";
                txtAsunto.Text = "";
            }
            catch (Exception eLE)
            {
                ShowMessage("Existe un error en limpiarModalEmail: " + eLE.Message, MessageType.Error, "alert_container");
            }
        }

        protected void btnGuardarCorreo_Click(object sender, EventArgs e)
        {
            try
            {
                clsEstudio mdlEstudio = new clsEstudio();
                mdlEstudio = lstCompleto.First(x => x.intEstudioID == Convert.ToInt32(lblIntEstudio.Text.ToString()));
                clsCorreo correo = new clsCorreo();
                correo.asunto = txtAsunto.Text;
                correo.toEmail = txtEmails.Text;
                string accNum = lblEstudio.Text;
                string url = "";
                url = MobilityAuthentication.dameLinkURLMobility(accNum.ToUpper());
                correo.NumAcc = accNum.ToUpper();
                correo.urlMensaje = url;
                string txtCorreo = "";
                txtCorreo = obtenerMachote();
                string token = MobilityAuthentication.createToken();
                string autenURL = ConfigurationManager.AppSettings["complementoURL"].ToString().Replace("TOKEN", token);
                string urlabrir1 = "";
                urlabrir1 = url + autenURL;
                correo.htmlCorreo = txtCorreo.Replace("URLSYN", (urlabrir1 != "" ? urlabrir1 : url));
                correo.correo = Security.Decrypt(ConfigurationManager.AppSettings["CorreoString"].ToString());
                correo.passwordCorreo = Security.Decrypt(ConfigurationManager.AppSettings["PassString"].ToString());
                string interpretacion = getInterpretacion(accNum);
                interpretacion = interpretacion == null ? "" : interpretacion;
                interpretacion = interpretacion.Replace("<p>", "");
                interpretacion = interpretacion.Replace("</p>", "");
                if (mdlEstudio != null)
                {
                    correo.Edad = mdlEstudio.vchEdad == null ? "" : mdlEstudio.vchEdad;
                    correo.FechaEstudio = mdlEstudio.datFecha == null ? DateTime.MinValue : mdlEstudio.datFecha;
                    correo.FechaNacimiento = mdlEstudio.vchPatientBirthDate == null ? "" : mdlEstudio.vchPatientBirthDate;
                    correo.Genero = mdlEstudio.vchGenero == null ? "" : mdlEstudio.vchGenero;
                    correo.Interpretacion = interpretacion == null ? "" : interpretacion;
                    correo.PatientID = mdlEstudio.PatientID == null ? "" : mdlEstudio.PatientID;
                    correo.PatientName = mdlEstudio.PatientName == null ? "" : mdlEstudio.PatientName;
                    correo.bitReporte = chkReporte.Checked;
                }
                if (enviarCorreo(correo))
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalEmail", "$('#modalEmail').modal('hide');", true);
                    limpiarControles();
                    ShowMessage("Correo enviado correctamente", MessageType.Success, "alert_container");
                }
                else
                {
                    ShowMessage("El correo no se pudo enviar, favor de revisar la información", MessageType.Error, "alert_container");
                }
            }
            catch (Exception eEC)
            {
                ShowMessage("Existe un error en Enviar correo: " + eEC.Message, MessageType.Error, "alert_container");
            }
        }

        private string getInterpretacion(string accNum)
        {
            string inter = "";
            try
            {
                string serverpath = Server.MapPath("~/WPFService/Correcto/" + accNum + ".html");
                if (File.Exists(serverpath))
                {
                    inter = HtmlToPlainText(File.ReadAllText(serverpath));
                }
            }
            catch (Exception egI)
            {
                Log.EscribeLog("Existe un error al obtener la interpretacion: " + egI.Message);
            }
            return inter;
        }

        private string obtenerMachote()
        {
            string texto = "";
            try
            {
                if (File.Exists(Server.MapPath("~/Data/machotecorreo.txt")))
                {
                    texto = File.ReadAllText(Server.MapPath("~/Data/machotecorreo.txt"));
                }
                else
                {
                    texto = "<table width='350px' style='FONT-SIZE:11px;font-family:Tahoma,Helvetica,sans-serif;padding:2px;background-color:#fdfffe;BORDER-RIGHT:#0c922e 3px solid;BORDER-TOP:#0c922e 3px solid;BORDER-LEFT:#0c922e 3px solid;BORDER-BOTTOM:#0c922e 3px solid'>" +
                            "<tbody>" +
                            "<tr><td colspan='2'>FEED2CLOUD</td></tr><tr><td colspan='2'><hr></td></tr><tr><td colspan='2' align='center' style='background-color:#fefefe'>VISUALIZAR <span class='il'>ESTUDIO</span></td></tr>" +
                            "<tr><td colspan='2'><hr></td></tr>" +
                            "<tr><td colspan='2'>Apreciable doctor:</td></tr>" +
                            "<tr><td colspan='2'>Para acceder a visualizar los estudios de su paciente ingrese a la siguiente dirección:</td></tr>" +
                            "<tr><td colspan='2'><center><b><a href='URLSYN'>Ir al estudio</a></b></center></td></tr>" +
                            "<tr><td align='center' colspan='2'><font color='#014615' size='3'>FUJIFILM MEXICO</font></td></tr>" +
                            "<tr><td colspan='2'>Este correo&nbsp; electronico&nbsp; es&nbsp; confidencial, esta&nbsp; legalmente&nbsp; protegido y/o puede contener informacion privilegiada. Si usted no es su destinatario o no es alguna persona autorizada por este para recibir sus correos electronicos, NO debera usted utilizar, copiar, revelar, o&nbsp; tomar&nbsp; ninguna&nbsp; accion&nbsp; basada&nbsp; en este correo electronico o cualquier otra informacion incluida en el (incluyendo todos los documentos adjuntos). </td></tr>" +
                            "</tbody></table>";
                }
            }
            catch (Exception eoM)
            {
                texto = "<table width='350px' style='FONT-SIZE:11px;font-family:Tahoma,Helvetica,sans-serif;padding:2px;background-color:#fdfffe;BORDER-RIGHT:#0c922e 3px solid;BORDER-TOP:#0c922e 3px solid;BORDER-LEFT:#0c922e 3px solid;BORDER-BOTTOM:#0c922e 3px solid'>" +
                            "<tbody>" +
                            "<tr><td colspan='2'>FEED2CLOUD</td></tr><tr><td colspan='2'><hr></td></tr><tr><td colspan='2' align='center' style='background-color:#fefefe'>VISUALIZAR <span class='il'>ESTUDIO</span></td></tr>" +
                            "<tr><td colspan='2'><hr></td></tr>" +
                            "<tr><td colspan='2'>Apreciable doctor:</td></tr>" +
                            "<tr><td colspan='2'>Para acceder a visualizar los estudios de su paciente ingrese a la siguiente dirección:</td></tr>" +
                            "<tr><td colspan='2'><center><b><a href='URLSYN'>Ir al estudio</a></b></center></td></tr>" +
                            "<tr><td align='center' colspan='2'><font color='#014615' size='3'>FUJIFILM MEXICO</font></td></tr>" +
                            "<tr><td colspan='2'>Este correo&nbsp; electronico&nbsp; es&nbsp; confidencial, esta&nbsp; legalmente&nbsp; protegido y/o puede contener informacion privilegiada. Si usted no es su destinatario o no es alguna persona autorizada por este para recibir sus correos electronicos, NO debera usted utilizar, copiar, revelar, o&nbsp; tomar&nbsp; ninguna&nbsp; accion&nbsp; basada&nbsp; en este correo electronico o cualquier otra informacion incluida en el (incluyendo todos los documentos adjuntos). </td></tr>" +
                            "</tbody></table>";
                Log.EscribeLog("Existe un error al obtener el machote del correo: " + eoM.Message);
            }
            return texto;
        }

        private bool enviarCorreo(clsCorreo correo)
        {
            bool valido = false;
            try
            {
                Thread envioCorreos = new Thread(() =>
                {
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(correo.correo);
                    string[] lista_correos = correo.toEmail.Split(';');

                    foreach (string destino in lista_correos)
                    {
                        mail.To.Add(destino);
                    }
                    mail.Subject = correo.asunto;
                    mail.IsBodyHtml = true;
                    mail.Body = correo.htmlCorreo;

                    if (correo.bitReporte)
                    {
                        try
                        {
                            mail.Attachments.Add(CreatePDF(correo));
                        }
                        catch (Exception e)
                        {
                            Log.EscribeLog("Existe un error al adjuntar el pdf: " + e.Message);
                        }
                    }
                    try
                    {
                        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                        smtp.Credentials = new System.Net.NetworkCredential(correo.correo, correo.passwordCorreo);

                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        smtp.Send(mail);
                    }
                    catch (Exception except)
                    {
                        valido = false;
                        Log.EscribeLog("Existe un error al intentar enviar el correo: " + except.Message);
                    }
                });
                envioCorreos.Start();
                valido = true;
            }
            catch (Exception eeC)
            {
                Log.EscribeLog("Existe un error en enviarCorreo: " + eeC.Message);
                valido = false;
            }
            return valido;
        }

        private Attachment CreatePDF(clsCorreo NumAcc)
        {
            Attachment pdfAtt = null;
            try
            {
                // Variables
                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;

                DataSet dsData = new DataSet();
                List<clsCorreo> lstC = new List<clsCorreo>();
                lstC.Add(NumAcc);
                dsData = DataSets.ToDataSet(lstC);
                ReportDataSource rds = new ReportDataSource("ReportSource", dsData.Tables[0]);
                // Setup the report viewer object and get the array of bytes
                ReportViewer viewer = new ReportViewer();
                viewer.LocalReport.Refresh();
                viewer.ProcessingMode = ProcessingMode.Local;
                string pathreport = Server.MapPath("~/Data/Report1.rdlc");
                Log.EscribeLog("Ruta del Reporte: " + pathreport);
                viewer.LocalReport.ReportPath = pathreport;
                ReportParameter p1 = new ReportParameter("NumAcc", NumAcc.NumAcc);
                ReportParameter p2 = new ReportParameter("FechaEstudio", NumAcc.FechaEstudio.ToString("dd/MM/yyyy HH:mm"));
                ReportParameter p3 = new ReportParameter("PatientID", NumAcc.PatientID);
                ReportParameter p4 = new ReportParameter("PatientName", NumAcc.PatientName);
                ReportParameter p5 = new ReportParameter("Edad", NumAcc.Edad);
                ReportParameter p6 = new ReportParameter("Genero", NumAcc.Genero);
                ReportParameter p7 = new ReportParameter("FechaNac", NumAcc.FechaNacimiento);
                ReportParameter p8 = new ReportParameter("Interpretacion", NumAcc.Interpretacion);
                viewer.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5, p6, p7, p8 });
                Log.EscribeLog("Parametros: " + p1.Values + "," + p2.Values + "," + p3.Values + "," + p4.Values + "," + p5.Values + "," + p6.Values + "," + p7.Values + "," + p8.Values);
                viewer.LocalReport.DisplayName = "Reporte:";
                byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                var stream = new System.IO.MemoryStream(bytes);
                pdfAtt = new Attachment(stream, "Report_" + NumAcc.NumAcc + ".pdf");


                // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
                //Response.Buffer = true;
                //Response.Clear();
                //Response.ContentType = mimeType;
                //Response.AddHeader("content-disposition", "attachment; filename=" + NumAcc + "." + extension);
                //Response.BinaryWrite(bytes); // create the file
                //Response.Flush(); // send it to the client to download
            }
            catch (Exception eCPDF)
            {
                Log.EscribeLog("Existe un error al crear el PDF: " + eCPDF.Message + " *" + eCPDF.InnerException);
            }
            return pdfAtt;
        }

        private static string HtmlToPlainText(string html)
        {
            //try
            //{
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
            //}
            //catch(Exception eHTML)
            //{
            //    Log.EscribeLog("Existe un error en : HtmlToPlainText : " + eHTML.Message);
            //}
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "mdlAvisos", "$('#mdlAvisos').modal('hide');", true);
        }

        protected void btnCloseexampleModal_Click(object sender, EventArgs e)
        {
            try
            {
                clsMensaje result = new clsMensaje();
                tbl_MST_Avisos aviso = new tbl_MST_Avisos();
                aviso = (tbl_MST_Avisos)Session["AvisoMDL"];
                MontebelloDA.setAvisoConfirmacion(aviso);
                cargarAviso(id_sitio);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "exampleModal", "$('#exampleModal').modal('hide');", true);
            }
            catch (Exception Ebc)
            {
                ShowMessage("Existe un error al completar el mensaje: " + Ebc.Message, MessageType.Error, "alert_container");
                Log.EscribeLog("Existe un error al completar el aviso: " + Ebc.Message);
            }
        }
    }
}