﻿using FUJI.Feed2.Entidades;
using FUJI.Montebello.AccesoDatos.DataAccess;
using FUJI.Montebello.Site.Services.DataContracts;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace FUJI.Montebello.Site.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IMontebelloService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IMontebelloService
    {
        [OperationContract]
        LoginResponse Logear(LoginRequest Request);

        [OperationContract]
        List<tbl_CAT_Modalidad> getCatModalidad();

        [OperationContract]
        clsMensaje getListEstudios(int id_Sitio, int intModalidadID, DateTime datInicio, DateTime datFinal);

        [OperationContract]
        clsMensaje getListEstudiosParam(int id_Sitio, int intModalidadID, DateTime datInicio, DateTime datFinal, string vchAccNum, string vchPatientID, string vchPatientName);

        [OperationContract]
        clsMensaje getEstudio(int intEstudioID);

        [OperationContract]
        clsMensaje setSolicitarInterpretacion(int intEstudioID);

        [OperationContract]
        tbl_MST_Avisos obtenerAvisos(int id_Sitio);

        [OperationContract]
        clsMensaje setAvisoConfirmacion(tbl_MST_Avisos aviso);
    }
}
