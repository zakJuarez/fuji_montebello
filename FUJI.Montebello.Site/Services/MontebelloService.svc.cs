﻿using FUJI.Feed2.Entidades;
using FUJI.Montebello.AccesoDatos;
using FUJI.Montebello.AccesoDatos.DataAccess;
using FUJI.Montebello.AccesoDatos.Extensions;
using FUJI.Montebello.Site.Services.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FUJI.Montebello.Site.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "MontebelloService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione MontebelloService.svc o MontebelloService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class MontebelloService : IMontebelloService
    {
        public LoginResponse Logear(LoginRequest Request)
        {
            LoginResponse Response = new LoginResponse();
            MontebelloDataAccess controller = new MontebelloDataAccess();
            clsUsuario entidad = new clsUsuario();
            try
            {
                Response.Success = controller.Logear(Request.username, Request.password, Request.sitio, ref entidad);
                Response.CurrentUser = entidad;
            }
            catch (Exception egV)
            {
                Log.EscribeLog("Error en Logear:" + egV.Message);
            }
            return Response;
        }

        public List<tbl_CAT_Modalidad> getCatModalidad()
        {
            List<tbl_CAT_Modalidad> Response = new List<tbl_CAT_Modalidad>();
            MontebelloDataAccess controller = new MontebelloDataAccess();
            try
            {
                Response = controller.getCatModalidad();
            }
            catch (Exception egV)
            {
                Log.EscribeLog("Error en getCatModalidad:" + egV.Message);
            }
            return Response;
        }

        public clsMensaje getListEstudios(int id_Sitio, int intModalidadID, DateTime datInicio, DateTime datFinal)
        {
            clsMensaje response = new clsMensaje();
            List<clsEstudio> lst = new List<clsEstudio>();
            try
            {
                MontebelloDataAccess controller = new MontebelloDataAccess();
                string mensaje = "";
                lst = controller.getListEstudios(id_Sitio, intModalidadID, datInicio, datFinal, ref mensaje);
                response.vchMensaje = mensaje;
                response._lstEst = lst;
            }
            catch (Exception egV)
            {
                Log.EscribeLog("Error en getListEstudios:" + egV.Message);
            }
            return response;
        }

        public clsMensaje getListEstudiosParam(int id_Sitio, int intModalidadID, DateTime datInicio, DateTime datFinal, string vchAccNum, string vchPatientID, string vchPatientName)
        {
            clsMensaje response = new clsMensaje();
            List<clsEstudio> lst = new List<clsEstudio>();
            try
            {
                MontebelloDataAccess controller = new MontebelloDataAccess();
                string mensaje = "";
                lst = controller.getListEstudiosParam(id_Sitio, intModalidadID, datInicio, datFinal, vchAccNum, vchPatientID, vchPatientName, ref mensaje);
                response.vchMensaje = mensaje;
                response._lstEst = lst;
            }
            catch (Exception egV)
            {
                Log.EscribeLog("Error en getListEstudiosParam:" + egV.Message);
            }
            return response;
        }

        public clsMensaje getEstudio(int intEstudioID)
        {
            clsMensaje response = new clsMensaje();
            clsEstudio lst = new clsEstudio();
            try
            {
                Log.EscribeLog("Obtinene el estudio: " + intEstudioID);
                MontebelloDataAccess controller = new MontebelloDataAccess();
                string mensaje = "";
                lst = controller.getEstudio(intEstudioID, ref mensaje);
                response.vchMensaje = mensaje;
                response.mdlEstudio = lst;
            }
            catch (Exception egV)
            {
                Log.EscribeLog("Error en getEstudio:" +  egV.Message);
            }
            return response;
        }

        public clsMensaje setSolicitarInterpretacion(int intEstudioID)
        {
            clsMensaje response = new clsMensaje();
            try
            {
                Log.EscribeLog("Obtinene el estudio: " + intEstudioID);
                MontebelloDataAccess controller = new MontebelloDataAccess();
                string mensaje = "";
                response.valido = controller.setSolicitarInterpretacion(intEstudioID, ref mensaje);
                response.vchMensaje = mensaje;
            }
            catch (Exception egV)
            {
                Log.EscribeLog("Error en setSolicitarInterpretacion:" + egV.Message);
            }
            return response;
        }

        public tbl_MST_Avisos obtenerAvisos(int id_Sitio)
        {
            tbl_MST_Avisos response = new tbl_MST_Avisos();
            try
            {
                Log.EscribeLog("Obtinene el obtenerAvisos: " + id_Sitio);
                MontebelloDataAccess controller = new MontebelloDataAccess();
                response = controller.obtenerAvisos(id_Sitio);
            }
            catch (Exception egV)
            {
                Log.EscribeLog("Error en obtenerAvisos:" + egV.Message);
            }
            return response;
        }

        public clsMensaje setAvisoConfirmacion(tbl_MST_Avisos aviso)
        {
            clsMensaje response = new clsMensaje();
            try
            {
                MontebelloDataAccess controller = new MontebelloDataAccess();
                string mensaje = "";
                response.valido = controller.setAvisoConfirmacion(aviso, ref mensaje);
                response.vchMensaje = mensaje;
            }
            catch (Exception egV)
            {
                Log.EscribeLog("Error en setAvisoConfirmacion:" + egV.Message);
            }
            return response;
        }
        



    }
}
