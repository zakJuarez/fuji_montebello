﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FUJI.Montebello.Site.Services.DataContracts
{
    public class LoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string sitio { get; set;}
    }
}