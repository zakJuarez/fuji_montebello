﻿using System;

namespace FUJI.Feed2.Entidades
{
    public class clsEstudio
    {
        public int intEstudioID { get; set; }
        public int intProyectoID { get; set; }
        public int id_Sitio { get; set; }
        public string vchClaveSitio { get; set; }
        public int intModalidadID { get; set; }
        public string vchModalidadID { get; set; }
        public string vchAccessionNumber { get; set; }
        public string vchPatientBirthDate { get; set; }
        public string PatientID { get; set; }
        public string PatientName { get; set; }
        public string vchEdad { get; set; }
        public string vchGenero { get; set; }
        public DateTime datFecha { get; set; }
        public DateTime datFechaIni { get; set; }
        public DateTime datFechaFin { get; set; }
        public int intEstatusID { get; set; }
        public string vchEstatusID { get; set; }
        public string vchCarpetaSyn { get; set; }
        //public bool bitSolInterpretacion { get; set; }
        public DateTime datFechaEstudio { get; set; }

        public clsEstudio()
        {
            intEstudioID = int.MinValue;
            intProyectoID = int.MinValue;
            id_Sitio = int.MinValue;
            vchClaveSitio = string.Empty;
            intModalidadID = int.MinValue;
            vchModalidadID = string.Empty;
            vchAccessionNumber = string.Empty;
            vchPatientBirthDate = string.Empty;
            PatientID = string.Empty;
            PatientName = string.Empty;
            vchEdad = string.Empty;
            vchGenero = string.Empty;
            datFecha = DateTime.MinValue;
            datFechaIni = DateTime.MinValue;
            datFechaFin = DateTime.MinValue;
            intEstatusID = int.MinValue;
            vchEstatusID = string.Empty;
            vchCarpetaSyn = string.Empty;
            //bitSolInterpretacion = false;
            datFechaEstudio = DateTime.MinValue;
        }
    }
}
