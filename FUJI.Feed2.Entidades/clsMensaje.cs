﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FUJI.Feed2.Entidades
{
    public class clsMensaje
    {
        public string vchMensaje { get; set; }
        public string vchError { get; set; }
        public bool valido { get; set; }
        public List<clsEstudio> _lstEst;
        public clsEstudio mdlEstudio;
        public clsMensaje()
        {
            _lstEst = new List<clsEstudio>();
            mdlEstudio = new clsEstudio();
            vchError = string.Empty;
            vchMensaje = string.Empty;
            valido = false;
        }
    }
}
